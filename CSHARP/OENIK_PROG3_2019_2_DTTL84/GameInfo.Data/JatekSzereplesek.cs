// <copyright file="JatekSzereplesek.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Characters in games.
    /// </summary>
    [Table("JatekSzereplesek")]
    public partial class JatekSzereplesek
    {
        /// <summary>
        /// Gets or sets Game id.
        /// </summary>
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Jatek_id { get; set; }

        /// <summary>
        /// Gets or sets Character id.
        /// </summary>
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Karakter_id { get; set; }

        /*
        public virtual Jatekok Jatekok { get; set; }

        public virtual Karakterek Karakterek { get; set; }

        public override string ToString()
        {
            return $"Karakter_ID = {Karakter_id}, Karakterek = {Jatek_id}";
        }*/
    }
}
