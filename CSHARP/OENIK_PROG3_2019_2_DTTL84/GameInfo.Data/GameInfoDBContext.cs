// <copyright file="GameInfoDBContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Data
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    /// <summary>
    /// Database Context.
    /// </summary>
    public partial class GameInfoDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameInfoDBContext"/> class.
        /// </summary>
        public GameInfoDBContext()
            : base("name=GameInfoDBContext")
        {
        }

        /// <summary>
        /// Gets or sets companies.
        /// </summary>
        public virtual DbSet<Cegek> Cegeks { get; set; }

        /// <summary>
        /// Gets or sets game expense.
        /// </summary>
        public virtual DbSet<Jatek_Kiadasok> Jatek_Kiadasok { get; set; }

        /// <summary>
        /// Gets or sets Games.
        /// </summary>
        public virtual DbSet<Jatekok> Jatekoks { get; set; }

        /// <summary>
        /// Gets or sets Characters.
        /// </summary>
        public virtual DbSet<Karakterek> Karaktereks { get; set; }

        /// <summary>
        /// Gets or sets Charactersin games.
        /// </summary>
        public virtual DbSet<JatekSzereplesek> JatekSzerepleseks { get; set; }

        /// <summary>
        /// When creating database modell.
        /// </summary>
        /// <param name="modelBuilder">Database modell.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cegek>()
                .Property(e => e.Ceg_nev)
                .IsUnicode(false);

            modelBuilder.Entity<Cegek>()
                .Property(e => e.Szekhely)
                .IsUnicode(false);

            modelBuilder.Entity<Cegek>()
                .Property(e => e.Fonok)
                .IsUnicode(false);

            modelBuilder.Entity<Cegek>()
                .HasMany(e => e.Cegek1)
                .WithOptional(e => e.Cegek2)
                .HasForeignKey(e => e.Anyaceg);

            modelBuilder.Entity<Cegek>()
                .HasMany(e => e.Jatekoks)
                .WithOptional(e => e.Cegek)
                .HasForeignKey(e => e.Keszito_id);

            modelBuilder.Entity<Cegek>()
                .HasMany(e => e.Jatekoks1)
                .WithOptional(e => e.Cegek1)
                .HasForeignKey(e => e.Kiado_id);

            modelBuilder.Entity<Jatek_Kiadasok>()
                .Property(e => e.Platform)
                .IsUnicode(false);

            modelBuilder.Entity<Jatekok>()
                .Property(e => e.Jatek_nev)
                .IsUnicode(false);

            modelBuilder.Entity<Jatekok>()
                .Property(e => e.Leiras)
                .IsUnicode(false);

            modelBuilder.Entity<Jatekok>()
                .Property(e => e.Game_engine)
                .IsUnicode(false);

            modelBuilder.Entity<Jatekok>()
                .Property(e => e.Sorozat)
                .IsUnicode(false);

            modelBuilder.Entity<Karakterek>()
                .Property(e => e.Karakter_nev)
                .IsUnicode(false);

            modelBuilder.Entity<Karakterek>()
                .Property(e => e.Faj)
                .IsUnicode(false);

            modelBuilder.Entity<Karakterek>()
                .Property(e => e.Nem)
                .IsUnicode(false);

            modelBuilder.Entity<Karakterek>()
                .Property(e => e.Szulohely)
                .IsUnicode(false);

            modelBuilder.Entity<Karakterek>()
                .Property(e => e.POP)
                .IsUnicode(false);
            /*
            modelBuilder.Entity<Karakterek>()
                .HasOptional(e => e.JatekSzereplesek)
                .WithRequired(e => e.Karakterek);
                */
        }
    }
}
