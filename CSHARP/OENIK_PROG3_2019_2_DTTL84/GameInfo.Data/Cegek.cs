﻿// <copyright file="Cegek.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Company database.
    /// </summary>
    [Table("Cegek")]
    public partial class Cegek
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Cegek"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors", Justification = "<Pending>")]
        public Cegek()
        {
            this.Cegek1 = new HashSet<Cegek>();
            this.Jatekoks = new HashSet<Jatekok>();
            this.Jatekoks1 = new HashSet<Jatekok>();
        }

        /// <summary>
        /// Gets or sets company id.
        /// </summary>
        [Key]
        public int Ceg_id { get; set; }

        /// <summary>
        /// Gets or sets company name.
        /// </summary>
        [StringLength(80)]
        public string Ceg_nev { get; set; }

        /// <summary>
        /// Gets or sets main city of the company.
        /// </summary>
        [StringLength(30)]
        public string Szekhely { get; set; }

        /// <summary>
        /// Gets or sets date of the inititation.
        /// </summary>
        public DateTime? Alapitas { get; set; }

        /// <summary>
        /// Gets or sets boss.
        /// </summary>
        [StringLength(60)]
        public string Fonok { get; set; }

        /// <summary>
        /// Gets or sets mother company.
        /// </summary>
        public int? Anyaceg { get; set; }

        /// <summary>
        /// Gets or sets numbers of workers.
        /// </summary>
        public int? Dolgozok_szama { get; set; }

        /// <summary>
        /// Gets or sets company1.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "<Pending>")]
        public virtual ICollection<Cegek> Cegek1 { get; set; }

        /// <summary>
        /// Gets or sets company2.
        /// </summary>
        public virtual Cegek Cegek2 { get; set; }

        /// <summary>
        /// Gets or sets Games1.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "<Pending>")]
        public virtual ICollection<Jatekok> Jatekoks { get; set; }

        /// <summary>
        /// Gets or sets másik Games2.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "<Pending>")]
        public virtual ICollection<Jatekok> Jatekoks1 { get; set; }
    }
}
