﻿// <copyright file="Jatekok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Games table.
    /// </summary>
    [Table("Jatekok")]
    public partial class Jatekok
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Jatekok"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors", Justification = "<Pending>")]
        public Jatekok()
        {
            this.JatekSzerepleseks = new HashSet<JatekSzereplesek>();
        }

        /// <summary>
        /// Gets or sets Game id.
        /// </summary>
        [Key]
        public int Jatek_id { get; set; }

        /// <summary>
        /// Gets or sets Game name.
        /// </summary>
        [StringLength(50)]
        public string Jatek_nev { get; set; }

        /// <summary>
        /// Gets or sets pegi.
        /// </summary>
        public short? Pegi { get; set; }

        /// <summary>
        /// Gets or sets Made by.
        /// </summary>
        public int? Keszito_id { get; set; }

        /// <summary>
        /// Gets or sets developed by.
        /// </summary>
        public int? Kiado_id { get; set; }

        /// <summary>
        /// Gets or sets title.
        /// </summary>
        [StringLength(200)]
        public string Leiras { get; set; }

        /// <summary>
        /// Gets or sets max players.
        /// </summary>
        public int? Max_players { get; set; }

        /// <summary>
        /// Gets or sets Game engine.
        /// </summary>
        [StringLength(20)]
        public string Game_engine { get; set; }

        /// <summary>
        /// Gets or sets name of the serie.
        /// </summary>
        [StringLength(50)]
        public string Sorozat { get; set; }

        /// <summary>
        /// Gets or sets Company1.
        /// </summary>
        public virtual Cegek Cegek { get; set; }

        /// <summary>
        /// Gets or sets Company2.
        /// </summary>
        public virtual Cegek Cegek1 { get; set; }

        /// <summary>
        /// Gets or sets Characters in games.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "<Pending>")]
        public virtual ICollection<JatekSzereplesek> JatekSzerepleseks { get; set; }
    }
}
