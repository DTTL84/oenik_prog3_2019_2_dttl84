// <copyright file="Jatek_Kiadasok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Game come outs.
    /// </summary>
    public partial class Jatek_Kiadasok
    {
        /// <summary>
        /// Gets or sets game id.
        /// </summary>
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Jatek_id { get; set; }

        /// <summary>
        /// Gets or sets platform.
        /// </summary>
        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string Platform { get; set; }

        /// <summary>
        /// Gets or sets Game come out.
        /// </summary>
        [Column(TypeName = "date")]
        public DateTime? Kiadas { get; set; }

        /// <summary>
        /// Gets or sets Store.
        /// </summary>
        public int? Tarhely { get; set; }
    }
}
