// <copyright file="Karakterek.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Character table.
    /// </summary>
    [Table("Karakterek")]
    public partial class Karakterek
    {
        /// <summary>
        /// Gets or sets Character id.
        /// </summary>
        [Key]
        public int Karakter_id { get; set; }

        /// <summary>
        /// Gets or sets character name.
        /// </summary>
        [StringLength(50)]
        public string Karakter_nev { get; set; }

        /// <summary>
        /// Gets or sets Character species.
        /// </summary>
        [Required]
        [StringLength(20)]
        public string Faj { get; set; }

        /// <summary>
        /// Gets or sets sex.
        /// </summary>
        [Required]
        [StringLength(7)]
        public string Nem { get; set; }

        /// <summary>
        /// Gets or sets born where.
        /// </summary>
        [StringLength(20)]
        public string Szulohely { get; set; }

        /// <summary>
        /// Gets or sets pop.
        /// </summary>
        [Required]
        [StringLength(1)]
        public string POP { get; set; }

        /// <summary>
        /// Gets or sets Characters in games.
        /// </summary>
        public virtual JatekSzereplesek JatekSzereplesek { get; set; }
    }
}
