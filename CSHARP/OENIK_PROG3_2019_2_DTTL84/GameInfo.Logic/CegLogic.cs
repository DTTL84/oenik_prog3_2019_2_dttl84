﻿// <copyright file="CegLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository;
    using GameInfo.Repository.Interfaces;

    public interface ICegLogic
    {
        void InsertNewCeg(Cegek newceg);
        Cegek GetOneCeg(int id);
        IList<Cegek> GetAllCompanies();
        bool ChangeCompany(int id, DateTime alapitas, string newName, int newEmployees, string newBoss, string newSzekhely);
        bool RemoveCompany(int id);
    }

    /// <summary>
    /// Company logic.
    /// </summary>
    public class CegLogic : ICegLogic
    {
        private ICegRepository cegRepo;
        private GameInfoDBContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="CegLogic"/> class.
        /// </summary>
        public CegLogic()
        {
            this.cegRepo = new CegRepository(new GameInfoDBContext());
            this.db = new GameInfoDBContext();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CegLogic"/> class.
        /// </summary>
        /// <param name="cegRepo">Ceg Repository.</param>
        public CegLogic(ICegRepository cegRepo)
        {
            this.cegRepo = cegRepo;
        }

        /// <summary>
        /// Change the boss.
        /// </summary>
        /// <param name="ceg_id">Company id.</param>
        /// <param name="newFonok">New value.</param>
        public void ChangeCegFonok(int ceg_id, string newFonok)
        {
            this.cegRepo.ChangeFonok(ceg_id, newFonok);
        }

        /// <summary>
        /// Get One Company.
        /// </summary>
        /// <param name="ceg_id">Company id.</param>
        /// <returns>1 row Company.</returns>
        public Cegek GetOneCeg(int ceg_id)
        {
            try
            {
                return this.cegRepo.GetOne(ceg_id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Insert new Company.
        /// </summary>
        /// <param name="newCeg">New Company.</param>
        public void InsertNewCeg(Cegek newCeg)
        {
            this.cegRepo.Add(newCeg);
        }

        /// <summary>
        /// Delete Company.
        /// </summary>
        /// <param name="ceg_id">Company id.</param>
        public void RemoveCeg(int ceg_id)
        {
            this.cegRepo.RemoveCompany(ceg_id);
        }

        /// <summary>
        /// Get All Company.
        /// </summary>
        /// <returns>All companies.</returns>
        public IList<Cegek> GetAllCegek()
        {
            return this.cegRepo.GetAll().ToList();
        }

        public IList<Cegek> GetAllCompanies()
        {
            return this.cegRepo.GetAll().ToList();
        }

        //public IList<Cegek> GetAllCegek()
        //{
        //    throw new NotImplementedException();
        //}

        public bool ChangeCompany(int id, DateTime alapitas, string newName, int newEmployees, string newBoss, string newSzekhely)
        {
            //this.cegRepo.ChangeCompany(ceg);
            var comp = cegRepo.GetAll().SingleOrDefault(x => x.Ceg_id == id);
            return cegRepo.ChangeCompany(id, alapitas, newName, newEmployees, newBoss, newSzekhely);
        }

        public bool RemoveCompany(int id)
        {
            //this.cegRepo.RemoveCeg(ceg_id);
            return cegRepo.RemoveCompany(id);
        }
    }
}
