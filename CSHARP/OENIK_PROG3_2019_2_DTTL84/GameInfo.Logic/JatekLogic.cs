﻿// <copyright file="JatekLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Game logic.
    /// </summary>
    public class JatekLogic
    {
        /// <summary>
        /// Game Repository.
        /// </summary>
        private readonly IJatekRepository jatekRepo;

        private GameInfoDBContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="JatekLogic"/> class.
        /// Constructor.
        /// </summary>
        public JatekLogic()
        {
            this.jatekRepo = new JatekRepository(new GameInfoDBContext());
            this.db = new GameInfoDBContext();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JatekLogic"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="jatekRepo">Játk Repo Interfész.</param>
        public JatekLogic(IJatekRepository jatekRepo)
        {
            this.jatekRepo = jatekRepo;
        }

        /// <summary>
        /// Change name.
        /// </summary>
        /// <param name="id">Game id.</param>
        /// <param name="newNev">New name.</param>
        public void ChangeNev(int id, string newNev)
        {
            this.jatekRepo.ChangeNev(id, newNev);
        }

        /// <summary>
        /// Get all games.
        /// </summary>
        /// <returns>all games.</returns>
        public IList<Jatekok> GetAllGames()
        {
            return this.jatekRepo.GetAll().ToList();
        }

        /// <summary>
        /// one game.
        /// </summary>
        /// <param name="game_id">Game id.</param>
        /// <returns>One row game.</returns>
        public Jatekok GetOneGame(int game_id)
        {
            return this.jatekRepo.GetOne(game_id);
        }

        /// <summary>
        /// Insert new game.
        /// </summary>
        /// <param name="newJatek">New gamek.</param>
        public void InsertNewJatek(Jatekok newJatek)
        {
            this.jatekRepo.Add(newJatek);
        }

        /// <summary>
        /// Delete game.
        /// </summary>
        /// <param name="id">Game id.</param>
        public void JatekEltavolitasa(int id)
        {
            this.jatekRepo.RemoveJatek(id);
        }

        ///// <summary>
        ///// Games from the company where workers less than x.
        ///// </summary>
        ///// <param name="dolgozok">Number of workers.</param>
        ///// <returns>Game list.</returns>
        // public IList<Jatekok> GetAdottCegAltalKeszitettJatekok(int dolgozok)
        // {
        //    var vissza = from x in this.db.Jatekoks
        //                 join c in this.db.Cegeks
        //                 on x.Keszito_id equals c.Ceg_id
        //                 where c.Dolgozok_szama <= dolgozok
        //                 select x;

        // return vissza.ToList();
        // }
    }
}
