﻿// <copyright file="JatekSzereplesLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Characters in games logic.
    /// </summary>
    public class JatekSzereplesLogic
    {
        /// <summary>
        /// Characters in games Repository.
        /// </summary>
        private readonly IJatekSzereplesekRepository jaszRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="JatekSzereplesLogic"/> class.
        /// Constructor.
        /// </summary>
        public JatekSzereplesLogic()
        {
            this.jaszRepo = new JatekSzereplesekRepository(new GameInfoDBContext());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JatekSzereplesLogic"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="jaszRepo">JátékSzereplés Repo Interfész.</param>
        public JatekSzereplesLogic(IJatekSzereplesekRepository jaszRepo)
        {
            this.jaszRepo = jaszRepo;
        }

        /// <summary>
        /// Get All Characters in games.
        /// </summary>
        /// <returns>All Characters in games.</returns>
        public IList<JatekSzereplesek> GetAllSzereples()
        {
            return this.jaszRepo.GetAll().ToList();
        }

        /// <summary>
        /// Insert new Characters in games.
        /// </summary>
        /// <param name="newSzerples">Új játék.</param>
        public void InsertNewJatek(JatekSzereplesek newSzerples)
        {
            this.jaszRepo.Add(newSzerples);
        }

        /// <summary>
        /// Remove Characters in games.
        /// </summary>
        /// <param name="kid">Character id.</param>
        /// <param name="jid">Game id.</param>
        public void JatekSzereplesEltavolitasa(int kid, int jid)
        {
            this.jaszRepo.RemoveSzereples(jid, kid);
        }
    }
}
