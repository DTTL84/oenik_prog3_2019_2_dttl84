﻿// <copyright file="KarakterLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Character Logic.
    /// </summary>
    public class KarakterLogic
    {
        /// <summary>
        /// Character Repository Interface.
        /// </summary>
        private IKarakterRepository karRepo;
        private GameInfoDBContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="KarakterLogic"/> class.
        /// Character Logic constructor.
        /// </summary>
        public KarakterLogic()
        {
            this.karRepo = new KarakterRepository(new GameInfoDBContext());
            this.db = new GameInfoDBContext();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KarakterLogic"/> class.
        /// Other Character Logic constructor.
        /// </summary>
        /// <param name="karRepo">Character Repo Interface.</param>
        public KarakterLogic(IKarakterRepository karRepo)
        {
            this.karRepo = karRepo;
        }

        /// <summary>
        /// Change Pop.
        /// </summary>
        /// <param name="id">Character id.</param>
        /// <param name="newPOP">New Value.</param>
        public void ChangePOP(int id, string newPOP)
        {
            this.karRepo.ChangePOP(id, newPOP);
        }

        /// <summary>
        /// Get all Characters.
        /// </summary>
        /// <returns>All Characters in the list.</returns>
        public IList<Karakterek> GetAllKarakter()
        {
            return this.karRepo.GetAll().ToList();
        }

        /// <summary>
        /// One Character.
        /// </summary>
        /// <param name="karakter_id">Character id.</param>
        /// <returns>One row Character.</returns>
        public Karakterek GetOneKarakter(int karakter_id)
        {
            return this.karRepo.GetOne(karakter_id);
        }

        /// <summary>
        /// Insert Character.
        /// </summary>
        /// <param name="newKarakter">New Character.</param>
        public void InsertKarakter(Karakterek newKarakter)
        {
            this.karRepo.Add(newKarakter);
        }

        /// <summary>
        /// Remove Character.
        /// </summary>
        /// <param name="id">Character id.</param>
        public void RemoveKarakter(int id)
        {
            this.karRepo.RemoveKarakter(id);
        }

        ///// <summary>
        ///// Characters from a game serie.
        ///// </summary>
        ///// <param name="sorozatneve">serie name.</param>
        ///// <returns>List.</returns>
        // public IGrouping<string,Karakterek> GetNevekSorozatbol(string sorozatneve)
        // {
        //    var asd = from x in this.db.Jatekoks
        //              join jsz in this.db.JatekSzerepleseks on x.Jatek_id equals jsz.Jatek_id
        //              join k in this.db.Karaktereks on jsz.Karakter_id equals k.Karakter_id
        //              where x.Sorozat.Contains(sorozatneve)
        //              group k by k.Karakter_nev into grp
        //              select grp;

        // return asd.ToList() as IGrouping<string, Karakterek>;
        // }
    }
}
