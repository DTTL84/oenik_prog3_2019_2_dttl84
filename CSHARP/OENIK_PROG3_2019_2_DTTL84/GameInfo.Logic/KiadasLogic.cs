﻿// <copyright file="KiadasLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Game out logic.
    /// </summary>
    public class KiadasLogic
    {
        private IKiadasRepository kiadasRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="KiadasLogic"/> class.
        /// </summary>
        public KiadasLogic()
        {
            this.kiadasRepo = new KiadasRepository(new GameInfoDBContext());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KiadasLogic"/> class.
        /// </summary>
        /// <param name="kiadasRepo">Game out Repository.</param>
        public KiadasLogic(IKiadasRepository kiadasRepo)
        {
            this.kiadasRepo = kiadasRepo;
        }

        /// <summary>
        /// Get all Game outs.
        /// </summary>
        /// <returns>All Game outs.</returns>
        public IList<Jatek_Kiadasok> GetAllKiadas()
        {
            return this.kiadasRepo.GetAll().ToList();
        }

        /// <summary>
        /// Get one Game out.
        /// </summary>
        /// <param name="jatek_id">Game id.</param>
        /// <param name="platform">Platform.</param>
        /// <returns>1 row Game out.</returns>
        public Jatek_Kiadasok GetOneKiadas(int jatek_id, string platform)
        {
            return this.kiadasRepo.GetOne(jatek_id, platform);
        }

        /// <summary>
        /// Insert a Game out.
        /// </summary>
        /// <param name="newkiadas">The new Game out.</param>
        public void InsertKiadas(Jatek_Kiadasok newkiadas)
        {
            this.kiadasRepo.Add(newkiadas);
        }

        /// <summary>
        /// Remove from Game out.
        /// </summary>
        /// <param name="jid">Game id.</param>
        /// <param name="platform">Platform.</param>
        public void RemoveKiadas(int jid, string platform)
        {
            this.kiadasRepo.RemoveKiadas(jid, platform);
        }

        /// <summary>
        /// Change come out date.
        /// </summary>
        /// <param name="jid">Game id.</param>
        /// <param name="platform">Platform.</param>
        /// <param name="newDate">new Date.</param>
        public void ChangeKiadas(int jid, string platform, DateTime newDate)
        {
            this.kiadasRepo.ChangeKiadas(jid, platform, newDate);
        }
    }
}
