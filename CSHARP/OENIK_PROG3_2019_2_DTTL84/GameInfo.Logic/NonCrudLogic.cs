﻿// <copyright file="NonCrudLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Non Crud Logic.
    /// </summary>
    public class NonCrudLogic
    {
        private ICegRepository cegRepo;
        private IJatekRepository jatekRepo;
        private IKarakterRepository karRepo;
        private IJatekSzereplesekRepository jszRepo;

        // private IKiadasRepository kiadRepo;
        private GameInfoDBContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCrudLogic"/> class.
        /// noncrud constructor.
        /// </summary>
        public NonCrudLogic()
        {
            this.cegRepo = new CegRepository(new GameInfoDBContext());
            this.jatekRepo = new JatekRepository(new GameInfoDBContext());
            this.karRepo = new KarakterRepository(new GameInfoDBContext());
            this.jszRepo = new JatekSzereplesekRepository(new GameInfoDBContext());

            // this.kiadRepo = new KiadasRepository(new GameInfoDBContext());
            this.db = new GameInfoDBContext();
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="NonCrudLogic"/> class.
        /// Noncrud logic constructor with parameters.
        /// </summary>
        /// <param name="cegRepo">Company Repository.</param>
        /// <param name="jatekRepo">Game repository.</param>
        /// <param name="karRepo">Character repository.</param>
        /// <param name="jszRepo">Characters in games repository.</param>
        public NonCrudLogic(ICegRepository cegRepo, IJatekRepository jatekRepo, IKarakterRepository karRepo, IJatekSzereplesekRepository jszRepo)
        {
            this.cegRepo = cegRepo;
            this.jatekRepo = jatekRepo;
            this.karRepo = karRepo;
            this.jszRepo = jszRepo;

            this.db = new GameInfoDBContext();

            // this.kiadRepo = kiadRepo;
        }
        
        /// <summary>
        /// Companies for Characters.
        /// </summary>
        /// <param name="karakter_id">Character id.</param>
        /// <returns>Commpany list.</returns>
        public IList<Cegek> KarakterhezCegek(int karakter_id)
        {
            var querry = from x in this.db.Cegeks
                         join y in this.db.Jatekoks
                         on x.Ceg_id equals y.Keszito_id
                         join z in this.db.JatekSzerepleseks
                         on y.Jatek_id equals z.Jatek_id
                         where z.Karakter_id == karakter_id
                         select x;

            // group x by x.Ceg_nev into grp
            return querry.ToList();
        }

        /// <summary>
        /// Games from the company where workers less than x.
        /// </summary>
        /// <param name="dolgozok">Number of workers.</param>
        /// <returns>Game list.</returns>
        public IList<Jatekok> GetAdottCegAltalKeszitettJatekok(int dolgozok)
        {
            var vissza = from x in this.db.Jatekoks
                         join c in this.db.Cegeks
                         on x.Keszito_id equals c.Ceg_id
                         where c.Dolgozok_szama <= dolgozok
                         select x;

            return vissza.ToList();
        }

        /// <summary>
        /// Characters from a game serie.
        /// </summary>
        /// <param name="sorozatneve">serie name.</param>
        /// <returns>List.</returns>
        public IList<string> GetNevekSorozatbol(string sorozatneve)
        {
            var asd = from z in this.db.JatekSzerepleseks
                      join x in this.db.Karaktereks on z.Karakter_id equals x.Karakter_id
                      join y in this.db.Jatekoks on z.Jatek_id equals y.Jatek_id
                      where y.Sorozat.Contains(sorozatneve)
                      group x by x.Karakter_nev into grp
                      select grp.Key;
            /*
            var asd = from x in this.jatekRepo.GetAll()
                      join jsz in this.db.JatekSzerepleseks on x.Jatek_id equals jsz.Jatek_id
                      //join k in this.karRepo.GetAll() on jsz.Karakter_id equals k.Karakter_id
                      where x.Sorozat.Contains(sorozatneve)
                      select jsz.Jatek_id;
                      */

            // group k by k.Karakter_nev into grp
            return asd.ToList();
        }
    }
}
