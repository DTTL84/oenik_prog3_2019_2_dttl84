﻿// <copyright file="NonCrud.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Logic;

    /// <summary>
    /// Non Crud.
    /// </summary>
    public class NonCrud
    {
        private static readonly string[] MenuValue = { "Karakterek Cégekhez", "Játékok a max dolgozókkal", "KarakterNevek Sorozatól" };

        /// <summary>
        /// Menu controll.
        /// </summary>
        public void MenuContoll()
        {
            int valasz = 0;
            do
            {
                MainMenu();
                valasz = int.Parse(Console.ReadLine());
                Controller(valasz);
            }
            while (valasz != 4);
            Console.Clear();
        }

        private static void MainMenu()
        {
            int szam = 0;
            for (int i = 0; i < MenuValue.Length; i++)
            {
                Console.WriteLine((i + 1) + " - " + MenuValue[szam]);
                szam++;
            }

            Console.WriteLine("Válasssz egy menüpontot");
        }

        private static void Controller(int valasz)
        {
            NonCrudLogic nc = new NonCrudLogic();
            switch (valasz)
            {
                case 1:
                    Console.WriteLine("Karakter azonosítója:");
                    int kid = int.Parse(Console.ReadLine());
                    foreach (var item in nc.KarakterhezCegek(kid))
                    {
                        Console.WriteLine(item.Ceg_nev);
                    }

                    break;
                case 2:
                    Console.WriteLine("Add meg a maximum dolgozók számát:");
                    int cid = int.Parse(Console.ReadLine());
                    foreach (var item in nc.GetAdottCegAltalKeszitettJatekok(cid))
                    {
                        Console.WriteLine(item.Jatek_nev);
                    }

                    break;
                case 3:
                    Console.WriteLine("Add meg a sorozat nevét:");
                    string sorozat = Console.ReadLine();
                    foreach (var item in nc.GetNevekSorozatbol(sorozat))
                    {
                        Console.WriteLine(item);
                    }

                    break;
                default:
                    Console.WriteLine("Nnics ilyen érték.");
                    break;
            }
        }
    }
}
