﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <summary>
    /// Menu Program.
    /// </summary>
    public class Menu
    {
        private static readonly string[] MenuValue = { "CRUD", "NEM CRUD LEKÉRDEZÉSEK", "JAVA INTERAKCIÓ", "EXIT" };

        /// <summary>
        /// Menu kontroll.
        /// </summary>
        public static void MenuContoll()
        {
            int valasz = 0;
            do
            {
                MainMenu();
                valasz = int.Parse(Console.ReadLine());
                Controller(valasz);

                Console.Clear();
            }
            while (valasz != 4);
            Console.WriteLine("Good bye! :)");
        }

        private static void MainMenu()
        {
            int szam = 0;
            for (int i = 0; i < MenuValue.Length; i++)
            {
                Console.WriteLine((i + 1) + " - " + MenuValue[szam]);
                szam++;
            }

            Console.WriteLine("Válasssz egy menüpontot");
        }

        private static void Controller(int valasz)
        {
            switch (valasz)
            {
                case 1:
                    Crud cr = new Crud();
                    cr.CrudControll();
                    break;
                case 2:
                    NonCrud ncr = new NonCrud();
                    ncr.MenuContoll();
                    break;
                case 3:
                    string url = $"http://localhost:8080/GameInfo.JavaWeb/GameInfo";
                    XDocument xDoc = XDocument.Load(url);
                    Console.WriteLine(xDoc.Root.Element("szam").Value);
                    Console.ReadLine();
                    break;
                case 4:
                    Console.WriteLine("Viszlát!");
                    break;
                default:
                    Console.WriteLine("Nnics ilyen válasz lehetőség!");
                    break;
            }
        }
    }
}
