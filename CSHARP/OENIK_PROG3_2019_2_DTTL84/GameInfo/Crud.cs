﻿// <copyright file="Crud.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Logic;

    /// <summary>
    /// Crud mthods.
    /// </summary>
    public class Crud
    {
        private static readonly string[] CrudValue = { "Cégek", "Játékok", "Karakterek", "Játék Szerplések", "Kiadások", "Exit" };
        private static readonly string[] Muveletek = { "GetAll", "GetOne", "Insert", "Update", "Remove" };

        /// <summary>
        /// Controller.
        /// </summary>
        public void CrudControll()
        {
            int valasz;
            int valasz2;
            do
            {
                Menu();
                valasz = int.Parse(Console.ReadLine());
                Muvelet();
                valasz2 = int.Parse(Console.ReadLine());
                this.Controller(valasz, valasz2);
            }
            while (valasz != 6);
        }

        /// <summary>
        /// Menu display.
        /// </summary>
        private static void Menu()
        {
            int szam = 0;
            for (int i = 0; i < CrudValue.Length; i++)
            {
                Console.WriteLine((i + 1) + " - " + CrudValue[szam]);
                szam++;
            }

            Console.WriteLine("Válasssz egy menüpontot");
        }

        /// <summary>
        /// Display methods.
        /// </summary>
        private static void Muvelet()
        {
            int szam = 0;
            for (int i = 0; i < Muveletek.Length; i++)
            {
                Console.WriteLine((i + 1) + " - " + Muveletek[szam]);
                szam++;
            }

            Console.WriteLine("Válasssz egy műveletet.");
        }

        private void Controller(int tabla, int muvelet)
        {
            switch (tabla)
            {
                case 1:
                    this.Cegekhez(muvelet);
                    break;
                case 2:
                    this.Jatekokhoz(muvelet);
                    break;
                case 3:
                    this.Karakterhez(muvelet);
                    break;
                case 4:
                    this.JatekSzerepleshez(muvelet);
                    break;
                case 5:
                    this.Kiadashoz(muvelet);
                    break;
                case 6:
                    Console.WriteLine("Viszlát!");
                    break;
                default:
                    Console.WriteLine("Nnics ilyen válasz lehetőség!");
                    this.CrudControll();
                    break;
            }
        }

        private void Kiadashoz(int muvelet)
        {
            KiadasLogic kl = new KiadasLogic();
            switch (muvelet)
            {
                case 1:
                    Console.WriteLine("GetAll");
                    foreach (var item in kl.GetAllKiadas())
                    {
                        Console.WriteLine("Játék id: " + item.Jatek_id + ", platform: " + item.Platform + " ,tárhely: " + item.Tarhely);
                    }

                    break;
                case 2:
                    Console.WriteLine("GetOne");
                    Console.WriteLine("Játék id:");
                    int jatek_id = int.Parse(Console.ReadLine());
                    Console.WriteLine("Platform:");
                    string platform = Console.ReadLine();
                    Console.WriteLine(kl.GetOneKiadas(jatek_id, platform).Kiadas);
                    break;
                case 3:
                    Console.WriteLine("Insert");
                    Console.WriteLine("Játék id:");
                    int jid = int.Parse(Console.ReadLine());
                    Console.WriteLine("Platform:");
                    string plat = Console.ReadLine();
                    Console.WriteLine("Tárhely:");
                    int tar = int.Parse(Console.ReadLine());
                    Console.WriteLine("Év:");
                    int ev = int.Parse(Console.ReadLine());
                    Console.WriteLine("Hónap:");
                    int honap = int.Parse(Console.ReadLine());
                    Console.WriteLine("Nap:");
                    int nap = int.Parse(Console.ReadLine());
                    Jatek_Kiadasok newKiadas = new Jatek_Kiadasok()
                    {
                        Jatek_id = jid,
                        Platform = plat,
                        Tarhely = tar,
                        Kiadas = new DateTime(ev, honap, nap),
                    };
                    kl.InsertKiadas(newKiadas);
                    break;
                case 4:
                    Console.WriteLine("Update");
                    Console.WriteLine("Játék id:");
                    int ujid = int.Parse(Console.ReadLine());
                    Console.WriteLine("Platform:");
                    string uplat = Console.ReadLine();
                    Console.WriteLine("Új kiadás év:");
                    int kiadev = int.Parse(Console.ReadLine());
                    Console.WriteLine("Új kiadás hónap:");
                    int kiadhonap = int.Parse(Console.ReadLine());
                    Console.WriteLine("Új kiadás nap:");
                    int kiadnap = int.Parse(Console.ReadLine());
                    kl.ChangeKiadas(ujid, uplat, new DateTime(kiadev, kiadhonap, kiadnap));
                    break;

                case 5:
                    Console.WriteLine("Remove");
                    Console.WriteLine("Játék id:");
                    int rjid = int.Parse(Console.ReadLine());
                    Console.WriteLine("Platform neve:");
                    string rplat = Console.ReadLine();
                    kl.RemoveKiadas(rjid, rplat);
                    break;
                default:
                    Console.WriteLine("Nnics ilyen válasz.");
                    System.Threading.Thread.Sleep(2000);
                    this.CrudControll();
                    break;
            }
        }

        private void JatekSzerepleshez(int muvelet)
        {
            JatekSzereplesLogic jszl = new JatekSzereplesLogic();
            switch (muvelet)
            {
                case 1:
                    Console.WriteLine("GetAll");
                    foreach (var item in jszl.GetAllSzereples())
                    {
                        Console.WriteLine("Játék id: " + item.Jatek_id + ", Karakter id: " + item.Karakter_id);
                    }

                    break;
                case 2:
                    Console.WriteLine("GetOne");
                    Console.WriteLine("Nincs ilyen");
                    break;
                case 3:
                    Console.WriteLine("Insert");
                    Console.WriteLine("Játék id:");
                    int jid = int.Parse(Console.ReadLine());
                    Console.WriteLine("Karakter id:");
                    int kid = int.Parse(Console.ReadLine());
                    JatekSzereplesek newJatekSzereples = new JatekSzereplesek()
                    {
                        Jatek_id = jid,
                        Karakter_id = kid,
                    };
                    jszl.InsertNewJatek(newJatekSzereples);
                    break;
                case 4:
                    Console.WriteLine("Update");
                    Console.WriteLine("Nnics ilyen");
                    break;

                case 5:
                    Console.WriteLine("Remove");
                    Console.WriteLine("Játék id:");
                    int rjid = int.Parse(Console.ReadLine());
                    Console.WriteLine("Karakter id:");
                    int rkid = int.Parse(Console.ReadLine());
                    jszl.JatekSzereplesEltavolitasa(rkid, rjid);
                    break;
                default:
                    Console.WriteLine("Nnics ilyen válasz.");
                    System.Threading.Thread.Sleep(2000);
                    this.CrudControll();
                    break;
            }
        }

        private void Karakterhez(int muvelet)
        {
            KarakterLogic kl = new KarakterLogic();
            switch (muvelet)
            {
                case 1:
                    Console.WriteLine("GetAll");
                    foreach (var item in kl.GetAllKarakter())
                    {
                        Console.WriteLine(item.Karakter_id + " Név: " + item.Karakter_nev);
                    }

                    break;
                case 2:
                    Console.WriteLine("GetOne");
                    Console.WriteLine("Melyik karakter akarod(id)?");
                    int id = int.Parse(Console.ReadLine());
                    Console.WriteLine(kl.GetOneKarakter(id).Karakter_nev);
                    break;
                case 3:
                    Console.WriteLine("Insert");
                    Console.WriteLine("Név:");
                    string nev = Console.ReadLine();
                    Console.WriteLine("Faj:");
                    string faj = Console.ReadLine();
                    Console.WriteLine("Nem (male/female)");
                    string nem = Console.ReadLine();
                    Console.WriteLine("Pop(yes/no):");
                    string pop = Console.ReadLine();
                    Console.WriteLine("Szülestési hely");
                    string hely = Console.ReadLine();
                    Karakterek newKarakter = new Karakterek()
                    {
                        Karakter_nev = nev,
                        Faj = faj,
                        Nem = nem,
                        POP = pop,
                        Szulohely = hely,
                    };
                    kl.InsertKarakter(newKarakter);
                    break;
                case 4:
                    Console.WriteLine("Update");
                    Console.WriteLine("Id:");
                    int cid = int.Parse(Console.ReadLine());
                    Console.WriteLine("Új Pop:");
                    string newpop = Console.ReadLine();
                    kl.ChangePOP(cid, newpop);
                    break;

                case 5:
                    Console.WriteLine("Remove");
                    Console.WriteLine("Karakter id:");
                    int rid = int.Parse(Console.ReadLine());
                    kl.RemoveKarakter(rid);
                    break;
                default:
                    Console.WriteLine("Nnics ilyen válasz.");
                    System.Threading.Thread.Sleep(2000);
                    this.CrudControll();
                    break;
            }
        }

        private void Jatekokhoz(int muvelet)
        {
            JatekLogic jl = new JatekLogic();
            switch (muvelet)
            {
                case 1:
                    Console.WriteLine("GetAll");
                    foreach (var item in jl.GetAllGames())
                    {
                        Console.WriteLine(item.Jatek_id + " Név: " + item.Jatek_nev);
                    }

                    break;
                case 2:
                    Console.WriteLine("GetOne");
                    Console.WriteLine("Melyik játékot akarod(id)?");
                    int id = int.Parse(Console.ReadLine());
                    Console.WriteLine(jl.GetOneGame(id).Jatek_nev);
                    break;
                case 3:
                    Console.WriteLine("Insert");
                    Console.WriteLine("Név:");
                    string nev = Console.ReadLine();
                    Console.WriteLine("Pegi:");
                    short pegi = short.Parse(Console.ReadLine());
                    Console.WriteLine("Készítő id:");
                    int keszito = int.Parse(Console.ReadLine());
                    Console.WriteLine("Kiadó id:");
                    int kiado = int.Parse(Console.ReadLine());
                    Console.WriteLine("Leírás:");
                    string leiras = Console.ReadLine();
                    Console.WriteLine("Max players:");
                    int max = int.Parse(Console.ReadLine());
                    Console.WriteLine("Game engine:");
                    string engine = Console.ReadLine();
                    Console.WriteLine("Sorozat:");
                    string sorozat = Console.ReadLine();

                    Jatekok newJatek = new Jatekok()
                    {
                        Jatek_nev = nev,
                        Pegi = pegi,
                        Keszito_id = keszito,
                        Kiado_id = kiado,
                        Leiras = leiras,
                        Max_players = max,
                        Game_engine = engine,
                        Sorozat = sorozat,
                    };
                    jl.InsertNewJatek(newJatek);
                    break;
                case 4:
                    Console.WriteLine("Update");
                    Console.WriteLine("Id:");
                    int cid = int.Parse(Console.ReadLine());
                    Console.WriteLine("Új Név:");
                    string newNev = Console.ReadLine();
                    jl.ChangeNev(cid, newNev);
                    break;

                case 5:
                    Console.WriteLine("Remove");
                    Console.WriteLine("Játék id:");
                    int rid = int.Parse(Console.ReadLine());
                    jl.JatekEltavolitasa(rid);
                    break;
                default:
                    Console.WriteLine("Nnics ilyen válasz.");
                    System.Threading.Thread.Sleep(2000);
                    this.CrudControll();
                    break;
            }
        }

        private void Cegekhez(int muvelet)
        {
            CegLogic cl = new CegLogic();
            switch (muvelet)
            {
                case 1:
                    Console.WriteLine("GetAll");
                    foreach (var item in cl.GetAllCegek())
                    {
                        Console.WriteLine(item.Ceg_id + " Név: " + item.Ceg_nev);
                    }

                    break;
                case 2:
                    Console.WriteLine("GetOne");
                    Console.WriteLine("Melyik céget akarod(id)?");
                    int id = int.Parse(Console.ReadLine());
                    Console.WriteLine(cl.GetOneCeg(id).Ceg_nev);
                    break;
                case 3:
                    Console.WriteLine("Insert");
                    Console.WriteLine("név:");
                    string nev = Console.ReadLine();
                    Console.WriteLine("Székhely:");
                    string hely = Console.ReadLine();
                    Console.WriteLine("Alapítás dátuma:");

                    Console.WriteLine("Főnök:");
                    string fonok = Console.ReadLine();
                    Console.WriteLine("Anyacég (id):");
                    int anya = int.Parse(Console.ReadLine());
                    Console.WriteLine("Dolgozók száma:");
                    int szam = int.Parse(Console.ReadLine());
                    Console.WriteLine("Alapítás éve:");
                    int ev = int.Parse(Console.ReadLine());
                    Console.WriteLine("Hónapja:");
                    int honap = int.Parse(Console.ReadLine());
                    Console.WriteLine("Nap:");
                    int nap = int.Parse(Console.ReadLine());

                    Cegek newCeg = new Cegek()
                    {
                        Ceg_nev = nev,
                        Szekhely = hely,
                        Fonok = fonok,
                        Anyaceg = anya,
                        Dolgozok_szama = szam,
                        Alapitas = new DateTime(ev, honap, nap),
                    };
                    cl.InsertNewCeg(newCeg);
                    break;
                case 4:
                    Console.WriteLine("Update");
                    Console.WriteLine("Id:");
                    int cid = int.Parse(Console.ReadLine());
                    Console.WriteLine("Új főnök:");
                    string newFonok = Console.ReadLine();
                    cl.ChangeCegFonok(cid, newFonok);
                    break;

                case 5:
                    Console.WriteLine("Remove");
                    Console.WriteLine("Cég id:");
                    int rid = int.Parse(Console.ReadLine());
                    cl.RemoveCeg(rid);
                    break;
                default:
                    Console.WriteLine("Nnics ilyen válasz.");
                    System.Threading.Thread.Sleep(2000);
                    this.CrudControll();
                    break;
            }
        }
    }
}
