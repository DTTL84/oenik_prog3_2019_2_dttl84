﻿INSERT INTO TIPUSOK(tipus_nev) VALUES ('stratégia');	
INSERT INTO TIPUSOK(tipus_nev) VALUES ('platform');		
INSERT INTO TIPUSOK(tipus_nev) VALUES ('horror');		
INSERT INTO TIPUSOK(tipus_nev) VALUES ('lövöldözős');	
INSERT INTO TIPUSOK(tipus_nev) VALUES ('sport');		
INSERT INTO TIPUSOK(tipus_nev) VALUES ('role-play');	
INSERT INTO TIPUSOK(tipus_nev) VALUES ('szimuláció');	
INSERT INTO TIPUSOK(tipus_nev) VALUES ('puzzle');		
INSERT INTO TIPUSOK(tipus_nev) VALUES ('akció-kaland');	



INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 	
('Spyro', 'sárkány', 'male', 'Artisans', 'yes');
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Crash Bandicoot', 'bandikút', 'male', null, 'yes');
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Ratchet', 'Lombax', 'male', null, 'yes');
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Clank', 'robot', 'male', 'Pokitaru', 'yes');
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Hunter', 'gepárd', 'male', 'Avalar', 'no');							
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Bianca', 'nyúl', 'female', null, 'no');
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Elora', 'faun', 'female', 'Avalar', 'no');
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Nathan Drake', 'ember', 'male', 'Egyesült Államok', 'yes');
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Cynder', 'sárkány', 'female', null, 'no');
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Captain Qwark', 'emberszerű', 'male', null, 'no');
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Ripto', 'dinosaur', 'male', null, 'no');
INSERT INTO Karakterek(KARAKTER_NEV, FAJ, NEM, SZULOHELY, POP) VALUES 
('Spider-man', 'ember', 'male', 'Egyesült Államok', 'yes');

INSERT INTO Cegek(CEG_NEV, SZEKHELY, ALAPITAS, FONOK, ANYACEG, DOLGOZOK_szama) VALUES 
('Sony Interactive Entertainment', 'Japán', '1993.11.16', 'Jim Ryan', null, 7000);
INSERT INTO Cegek(CEG_NEV, SZEKHELY, ALAPITAS, FONOK, ANYACEG, DOLGOZOK_szama) VALUES 
('Insomniac Games', 'Egyesült Államok', '1994.02.28', 'Ted Price', 1, 150);
INSERT INTO Cegek(CEG_NEV, SZEKHELY, ALAPITAS, FONOK, ANYACEG, DOLGOZOK_szama) VALUES 
('Naughty Dog', 'Egyesült Államok', '1984.09.27', 'Evan Wells', 1, 200);
INSERT INTO Cegek(CEG_NEV, SZEKHELY, ALAPITAS, FONOK, ANYACEG, DOLGOZOK_szama) VALUES 
('Activision Blizzad', 'Egyesült Államok', '2008.07.10', 'Coddy Johnson', null, 9900);
INSERT INTO Cegek(CEG_NEV, SZEKHELY, ALAPITAS, FONOK, ANYACEG, DOLGOZOK_szama) VALUES 
('Toys For Bob', 'Egyesült Államok', '1989.01.01', 'Fred Ford', 4, 0);
INSERT INTO Cegek(CEG_NEV, SZEKHELY, ALAPITAS, FONOK, ANYACEG, DOLGOZOK_szama) VALUES 
('Vicarious Visions', 'Egyesült Államok', '1990.01.01', 'Guha Bala', 4, 218);
INSERT INTO Cegek(CEG_NEV, SZEKHELY, ALAPITAS, FONOK, ANYACEG, DOLGOZOK_szama) VALUES 
('Sierra Entertainment', 'Egyesült Államok', '1979.01.01', 'Roberta Williams', 4, 300);


INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Spyro The Dragon', 7, 2, 2, 'Kalandozz végig egy kis lila sárkánnyal', 1, 'saját', 'Spyro');
INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Spyro Riptos Rage', 7, 2, 2, 'Kalandozz végig egy kis lila sárkánnyal2', 1, 'saját', 'Spyro');
INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Spyro Year Of The The Dragon', 7, 2, 2, 'Kalandozz végig egy kis lila sárkánnyal3', 1, 'saját', 'Spyro');
INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Spyro Dawn Of The Dragon', 12, 7, 7, 'Kalandozz végig egy kis lila sárkánnyal6', 2, 'saját', 'The Legend Of Spyro');
INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Spyro Reignited Trilogy', 7, 5, 4, 'Kalandozz végig egy kis lila sárkánnyal újra', 1, 'Unreal engine', 'Spyro');
INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Crash Bandicoot', 7, 3, 1, 'Bolondozz végig és győzz', 1, 'saját', 'Crash Bandicoot');
INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Crash Bandicoot N Sane Trilogy', 7, 5, 4, 'Bolondozz végig és győzz újra', 1, 'saját', 'Crash Bandicoot');
INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Crash Team Racing', 7, 5, 4, 'Versenyezz mindenkivel', 4, 'saját', 'Crash Bandicoot');
INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Marvels Spider Man', 12, 2, 1, 'Bújj Spider-man bőrébe és mentsd meg New Yorkot a bűnözőktől', 1, 'saját', null);
INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Ratchet and Clank', 7, 2, 1, 'Játsz Ratchet-tal és Clank-kal és utazz bolygókon és galaxisokon át', 1, 'saját', 'Ratchet and Clank');
INSERT INTO Jatekok(jatek_nev, pegi, keszito_id, kiado_id, leiras, max_players, Game_engine, Sorozat) VALUES
('Uncharted 4', 16, 3, 3, 'Felfedeztél már megannyi helyet, mégis mi jöhet még?', 1, 'saját', 'Uncharted');


INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(1, 9);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(1, 2);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(1, 8);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(2, 2);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(2, 8);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(2, 9);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(3, 9);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(3, 2);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(4, 9);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(5, 9);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(5, 2);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(6, 9);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(7, 9);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(8, 9);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(8, 5);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(8, 7);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(9, 9);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(10, 9);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(10, 2);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(10, 4);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(11, 4);
INSERT INTO JatekTipusai(jatek_id, tipus_id) VALUES(11, 9);


INSERT INTO JatekSzereplesek(Jatek_id, Karakter_id) VALUES(1, 1);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(2, 1);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(2, 5);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(2, 7);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(2, 11);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(3, 1);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(3, 5);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(3, 7);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(3, 11);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(3, 6);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(4, 1);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(4, 9);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(4, 5);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(5, 1);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(5, 5);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(5, 7);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(5, 11);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(5, 6);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(6, 2);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(7, 2);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(8, 1);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(8, 2);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(8, 5);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(9, 12);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(10, 3);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(10, 4);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(10, 10);
INSERT INTO JatekSzereplesek(JATEK_ID, KARAKTER_ID) Values(11, 8);


INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(1, 'PS1', '1998.09.10', 0.2);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(2, 'PS1', '1999.08.24', 0.4);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(3, 'PS1', '2000.05.15', 0.4);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(4, 'PS2', '2008.05.10', 1.8);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(4, 'PS3', '2008.05.10', 2.1);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(4, 'XBOX 360', '2008.05.10', 2.1);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(4, 'WII', '2008.05.10', 2.1);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(4, 'Nintendo DS', '2008.05.10', 1.6);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(5, 'PS4', '2018.10.04', 72.4);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(5, 'XBOX ONE', '2018.10.04', 72.4);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(5, 'Windows', '2019.06.21', 50.8);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(5, 'Nintendo Switch', '2019.06.21', 48.9);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(6, 'PS1', '1996.04.02', 0.3);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(7, 'PS4', '2017.06.30', 72);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(7, 'XBOX ONE', '2017.06.30', 72);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(7, 'Nintendo Switch', '2018.06.28', 69);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(7, 'Windows', '2018.06.28', 72);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(8, 'PS4', '2019.06.20', 66);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(8, 'XBOX ONE', '2019.06.20', 66);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(8, 'Nintendo Switch', '2019.06.20', 65);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(9, 'PS4', '2018.09.07', 70.4);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(10, 'PS4', '2016.04.12', 48.1);
INSERT INTO JATEK_KIADASOK(Jatek_id, Platform, Kiadas, Tarhely) VALUES
(11, 'PS4', '2016.05.10', 78.2);