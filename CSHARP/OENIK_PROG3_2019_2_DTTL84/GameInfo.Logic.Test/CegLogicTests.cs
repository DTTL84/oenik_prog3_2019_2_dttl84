﻿// <copyright file="CegLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Logic;
    using GameInfo.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Company Logic Test.
    /// </summary>
    [TestFixture]
    public class CegLogicTests
    {
        private Mock<ICegRepository> cegMock;
        private CegLogic ceglogic;
        private List<Cegek> cegek = new List<Cegek>()
        {
            new Cegek()
            {
                Ceg_id = 1,
                Ceg_nev = "asda",
                Alapitas = System.DateTime.Now,
                Anyaceg = null,
                Dolgozok_szama = 1000,
                Fonok = "Béla",
                Szekhely = "Budapest",
            },
        };

        /// <summary>
        /// Setup.
        /// </summary>
        [SetUp]
        public void SetUpRepository()
        {
            this.cegMock = new Mock<ICegRepository>(MockBehavior.Loose);
            this.ceglogic = new CegLogic(this.cegMock.Object);
            this.cegMock.Setup(x => x.GetAll()).Returns(this.cegek.AsQueryable());
        }

        /// <summary>
        /// Company GetAll Test.
        /// </summary>
        [Test]
        public void GetAll()
        {
            this.ceglogic.GetAllCegek();
            this.cegMock.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Delete Company test.
        /// </summary>
        [Test]
        public void DeleteCeg()
        {
            this.ceglogic.RemoveCeg(It.IsAny<int>());
            //this.cegMock.Verify(x => x.RemoveCeg(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// GetOne Company test.
        /// </summary>
        [Test]
        public void GetOneCeg()
        {
            this.ceglogic.GetOneCeg(It.IsAny<int>());
            this.cegMock.Verify(x => x.GetOne(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Insert New Company if null test.
        /// </summary>
        [Test]
        public void InsertNewCegNull()
        {
            this.ceglogic.InsertNewCeg(null);
            this.cegMock.Verify(x => x.Add(null), Times.Once);
        }

        /// <summary>
        /// Update Company test.
        /// </summary>
        [Test]
        public void UpdateCeg()
        {
            this.ceglogic.ChangeCegFonok(It.IsAny<int>(), It.IsAny<string>());
            this.cegMock.Verify(x => x.ChangeFonok(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Insert newCompany test.
        /// </summary>
        [Test]
        public void InsertNewCeg()
        {
            this.ceglogic.InsertNewCeg(It.IsAny<Cegek>());
            this.cegMock.Verify(x => x.Add(It.IsAny<Cegek>()), Times.Once);
        }

        ///// <summary>
        ///// Non crud test.
        ///// </summary>
        // [Test]
        // public void NonCrud()
        // {
        //    this.ceglogic.KarakterhezCegek(1);
        //    this.cegMock.Verify(x => x.GetAll(), Times.Once);
        // }
    }
}