﻿// <copyright file="JatekLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Logic;
    using GameInfo.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Game Logic Test.
    /// </summary>
    [TestFixture]
    public class JatekLogicTests
    {
        private Mock<IJatekRepository> jaMock;
        private JatekLogic jalogic;
        private List<Jatekok> jatekok = new List<Jatekok>()
        {
            new Jatekok()
            {
                Jatek_id = 1,
                Jatek_nev = "Alma",
                Game_engine = "unity",
                Keszito_id = 1,
                Kiado_id = 2,
                Leiras = "nagyon jó",
                Max_players = 4,
                Pegi = 12,
                Sorozat = "gyümölcs",
            },
        };

        /// <summary>
        /// Set Up Game.
        /// </summary>
        [SetUp]
        public void SetUpRepository()
        {
            this.jaMock = new Mock<IJatekRepository>(MockBehavior.Loose);
            this.jalogic = new JatekLogic(this.jaMock.Object);
            this.jaMock.Setup(x => x.GetAll()).Returns(this.jatekok.AsQueryable());
        }

        /// <summary>
        /// Game GetAll Test.
        /// </summary>
        [Test]
        public void GetAll()
        {
            this.jalogic.GetAllGames();
            this.jaMock.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Game delete test.
        /// </summary>
        [Test]
        public void DeleteJatek()
        {
            this.jalogic.JatekEltavolitasa(It.IsAny<int>());
            this.jaMock.Verify(x => x.RemoveJatek(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// GetOne Game test.
        /// </summary>
        [Test]
        public void GetOnJatek()
        {
            this.jalogic.GetOneGame(It.IsAny<int>());
            this.jaMock.Verify(x => x.GetOne(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Update Game test.
        /// </summary>
        [Test]
        public void UpdateJatek()
        {
            this.jalogic.ChangeNev(It.IsAny<int>(), It.IsAny<string>());
            this.jaMock.Verify(x => x.ChangeNev(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Insert Game test.
        /// </summary>
        [Test]
        public void InsertNewJatek()
        {
            this.jalogic.InsertNewJatek(It.IsAny<Jatekok>());
            this.jaMock.Verify(x => x.Add(It.IsAny<Jatekok>()), Times.Once);
        }

        ///// <summary>
        ///// Non CRUD test.
        ///// </summary>
        // [Test]
        // public void NonCrudTest()
        // {
        //    this.jalogic.GetAdottCegAltalKeszitettJatekok(It.IsAny<int>());
        // }
    }
}
