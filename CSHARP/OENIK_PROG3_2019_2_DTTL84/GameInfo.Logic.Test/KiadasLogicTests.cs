﻿// <copyright file="KiadasLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Logic;
    using GameInfo.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Come out Logic Test.
    /// </summary>
    [TestFixture]
    internal class KiadasLogicTests
    {
        private Mock<IKiadasRepository> kiMock;
        private KiadasLogic kilogic;
        private List<Jatek_Kiadasok> kiadasok = new List<Jatek_Kiadasok>()
        {
            new Jatek_Kiadasok()
            {
                Jatek_id = 1,
                Platform = "PS4",
                Tarhely = 10,
                Kiadas = System.DateTime.Now,
            },
        };

        /// <summary>
        /// Come out SetUp.
        /// </summary>
        [SetUp]
        public void SetUpRepository()
        {
            this.kiMock = new Mock<IKiadasRepository>();
            this.kilogic = new KiadasLogic(this.kiMock.Object);
            this.kiMock.Setup(x => x.GetAll()).Returns(this.kiadasok.AsQueryable());
        }

        /// <summary>
        /// GetAll Come out test.
        /// </summary>
        [Test]
        public void GetAll()
        {
            this.kilogic.GetAllKiadas();
            this.kiMock.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Delete Come out test.
        /// </summary>
        [Test]
        public void DeleteKiadas()
        {
            this.kilogic.RemoveKiadas(It.IsAny<int>(), It.IsAny<string>());
            this.kiMock.Verify(x => x.RemoveKiadas(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// GetOne Come out Test.
        /// </summary>
        [Test]
        public void GetOneKiadas()
        {
            this.kilogic.GetOneKiadas(It.IsAny<int>(), It.IsAny<string>());
            this.kiMock.Verify(x => x.GetOne(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
            /*
            var querry = this.kilogic.GetOneKiadas(1, "PS4");
            Assert.That(querry.Jatek_id, Is.EqualTo(1));
            Assert.That(querry.Platform, Is.EqualTo("PS4"));
            this.kiMock.Verify(x => x.GetOne(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
            */
        }

        /// <summary>
        /// Update Come out test.
        /// </summary>
        [Test]
        public void UpdateKiadas()
        {
            this.kilogic.ChangeKiadas(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>());
            this.kiMock.Verify(x => x.ChangeKiadas(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>()), Times.Once);
        }

        /// <summary>
        /// Insert ComeOut test.
        /// </summary>
        [Test]
        public void InsertKiadas()
        {
            this.kilogic.InsertKiadas(It.IsAny<Jatek_Kiadasok>());
            this.kiMock.Verify(x => x.Add(It.IsAny<Jatek_Kiadasok>()), Times.Once);
        }
    }
}
