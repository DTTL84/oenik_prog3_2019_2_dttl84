﻿// <copyright file="NonCrudLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Logic;
    using GameInfo.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tets only for noncrud.
    /// </summary>
    [TestFixture]
    class NonCrudLogicTest
    {
        private NonCrudLogic logic;

        private Mock<ICegRepository> fakecegRepo;
        private Mock<IJatekRepository> fakejatekRepo;
        private Mock<IKarakterRepository> fakekarRepo;
        private Mock<IJatekSzereplesekRepository> fakejszRepo;
        private Mock<IKiadasRepository> fakekiadRepo;




        ICegRepository cegRepository;
        IJatekRepository jatekRepository;
        IJatekSzereplesekRepository jszRepository;
        IKarakterRepository karRepository;



        /// <summary>
        /// Set up tests.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.fakecegRepo = new Mock<ICegRepository>(MockBehavior.Loose);
            this.fakejatekRepo = new Mock<IJatekRepository>(MockBehavior.Loose);
            this.fakekarRepo = new Mock<IKarakterRepository>(MockBehavior.Loose);
            this.fakejszRepo = new Mock<IJatekSzereplesekRepository>(MockBehavior.Loose);
            this.fakekiadRepo = new Mock<IKiadasRepository>(MockBehavior.Loose);

            List<JatekSzereplesek> szereplesek = new List<JatekSzereplesek>()
            {
            new JatekSzereplesek()
            {
                Karakter_id = 1,
                Jatek_id = 1,
            },
            };

            List<Cegek> cegek = new List<Cegek>()
            {
            new Cegek()
            {
                Ceg_id = 1,
                Ceg_nev = "asda",
                Alapitas = System.DateTime.Now,
                Anyaceg = null,
                Dolgozok_szama = 300,
                Fonok = "Béla",
                Szekhely = "Budapest",
            },
            };

            List<Jatekok> jatekok = new List<Jatekok>()
            {
            new Jatekok()
            {
                Jatek_id = 1,
                Jatek_nev = "Alma",
                Game_engine = "unity",
                Keszito_id = 1,
                Kiado_id = 1,
                Leiras = "nagyon jó",
                Max_players = 4,
                Pegi = 12,
                Sorozat = "Spyro",
            },
            };

            List<Karakterek> karakterek = new List<Karakterek>()
            {
            new Karakterek()
            {
                Karakter_id = 1,
                Faj = "sárkány",
                Karakter_nev = "Spyro",
                Nem = "male",
                POP = "yes",
                Szulohely = null,
            },
            };

            List<Jatek_Kiadasok> kiadasok = new List<Jatek_Kiadasok>()
            {
            new Jatek_Kiadasok()
            {
                Jatek_id = 1,
                Platform = "PS4",
                Tarhely = 10,
                Kiadas = System.DateTime.Now,
            },
            };
            
            this.cegRepository = this.fakecegRepo.Object;
            this.jatekRepository = this.fakejatekRepo.Object;
            this.jszRepository = this.fakejszRepo.Object;
            this.karRepository = this.fakekarRepo.Object;
            this.logic = new NonCrudLogic(this.cegRepository, this.jatekRepository, this.karRepository, this.jszRepository);

            this.fakecegRepo.Setup(m => m.GetAll()).Returns(cegek.AsQueryable);
            this.fakejatekRepo.Setup(m => m.GetAll()).Returns(jatekok.AsQueryable);
            this.fakejszRepo.Setup(m => m.GetAll()).Returns(szereplesek.AsQueryable);
            this.fakekarRepo.Setup(m => m.GetAll()).Returns(karakterek.AsQueryable);
        }

        /// <summary>
        /// Character with Conmpany test.
        /// </summary>
        [Test]
        public void NonCrudCharacterCompanyTest()
        {
            this.logic.KarakterhezCegek(1);
            this.fakekarRepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Company max employees and games.
        /// </summary>
        [Test]
        public void GetAdottCegAltalKeszitettJatekokTest()
        {
            this.logic.GetAdottCegAltalKeszitettJatekok(1000);
            this.fakecegRepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Get character names from serie.
        /// </summary>
        [Test]
        public void GetNevekSorozatbolTest()
        {
            this.logic.GetNevekSorozatbol("Spyro");
            this.fakekarRepo.Verify(x => x.GetAll(), Times.Once);
        }
    }
}
