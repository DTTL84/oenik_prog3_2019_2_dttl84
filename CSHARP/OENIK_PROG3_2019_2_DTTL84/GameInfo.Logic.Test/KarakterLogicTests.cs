﻿// <copyright file="KarakterLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Logic;
    using GameInfo.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Character logic test.
    /// </summary>
    [TestFixture]
    internal class KarakterLogicTests
    {
        private Mock<IKarakterRepository> karMock;
        private KarakterLogic karlogic;
        private List<Karakterek> karakterek = new List<Karakterek>()
        {
            new Karakterek()
            {
                Karakter_id = 1,
                Faj = "sárkány",
                Karakter_nev = "Spyro",
                Nem = "male",
                POP = "yes",
                Szulohely = null,
            },
        };

        /// <summary>
        /// SetUp Character Logic.
        /// </summary>
        [SetUp]
        public void SetUpRepository()
        {
            this.karMock = new Mock<IKarakterRepository>();
            this.karlogic = new KarakterLogic(this.karMock.Object);
            this.karMock.Setup(x => x.GetAll()).Returns(this.karakterek.AsQueryable());
        }

        /// <summary>
        /// Character GetAll test.
        /// </summary>
        [Test]
        public void GetAll()
        {
            this.karlogic.GetAllKarakter();
            this.karMock.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Delete Character test.
        /// </summary>
        [Test]
        public void DeleteKarakter()
        {
            this.karlogic.RemoveKarakter(It.IsAny<int>());
            this.karMock.Verify(x => x.RemoveKarakter(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// GetOne Character test.
        /// </summary>
        [Test]
        public void GetOneKarakter()
        {
            this.karlogic.GetOneKarakter(It.IsAny<int>());
            this.karMock.Verify(x => x.GetOne(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Update Character test.
        /// </summary>
        [Test]
        public void UpdateKarakter()
        {
            this.karlogic.ChangePOP(It.IsAny<int>(), It.IsAny<string>());
            this.karMock.Verify(x => x.ChangePOP(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Insert new Character test.
        /// </summary>
        [Test]
        public void InsertNewKarakter()
        {
            this.karlogic.InsertKarakter(It.IsAny<Karakterek>());
            this.karMock.Verify(x => x.Add(It.IsAny<Karakterek>()), Times.Once);
        }
    }
}
