var searchData=
[
  ['icegrepository_127',['ICegRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_ceg_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['ijatekrepository_128',['IJatekRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_jatek_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['ijatekszereplesekrepository_129',['IJatekSzereplesekRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_jatek_szereplesek_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['ikarakterrepository_130',['IKarakterRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_karakter_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['ikiadasrepository_131',['IKiadasRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_kiadas_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_132',['IRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_3c_20cegek_20_3e_133',['IRepository&lt; Cegek &gt;',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_3c_20jatek_5fkiadasok_20_3e_134',['IRepository&lt; Jatek_Kiadasok &gt;',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_3c_20jatekok_20_3e_135',['IRepository&lt; Jatekok &gt;',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_3c_20jatekszereplesek_20_3e_136',['IRepository&lt; JatekSzereplesek &gt;',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_3c_20karakterek_20_3e_137',['IRepository&lt; Karakterek &gt;',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]]
];
