var searchData=
[
  ['jatekeltavolitasa_197',['JatekEltavolitasa',['../class_game_info_1_1_logic_1_1_jatek_logic.html#a0ee679326c11af28d9fbc41d9409afd9',1,'GameInfo::Logic::JatekLogic']]],
  ['jateklogic_198',['JatekLogic',['../class_game_info_1_1_logic_1_1_jatek_logic.html#ab36cb8012824b9a70834fd8c0b72b727',1,'GameInfo.Logic.JatekLogic.JatekLogic()'],['../class_game_info_1_1_logic_1_1_jatek_logic.html#a566fb33cf8e8aa95c26a89d96b0a3bdc',1,'GameInfo.Logic.JatekLogic.JatekLogic(IJatekRepository jatekRepo)']]],
  ['jatekok_199',['Jatekok',['../class_game_info_1_1_data_1_1_jatekok.html#a227520a255c6e62c91a987df8eb3dc21',1,'GameInfo::Data::Jatekok']]],
  ['jatekrepository_200',['JatekRepository',['../class_game_info_1_1_repository_1_1_jatek_repository.html#a9128ca70cae66834f573340c3401b936',1,'GameInfo::Repository::JatekRepository']]],
  ['jatekszereplesekrepository_201',['JatekSzereplesekRepository',['../class_game_info_1_1_repository_1_1_jatek_szereplesek_repository.html#a831a14acefe9f76db9f1cadc2657f773',1,'GameInfo::Repository::JatekSzereplesekRepository']]],
  ['jatekszerepleseltavolitasa_202',['JatekSzereplesEltavolitasa',['../class_game_info_1_1_logic_1_1_jatek_szereples_logic.html#a9df0204e96de22fdbc7824ae3e3310ca',1,'GameInfo::Logic::JatekSzereplesLogic']]],
  ['jatekszerepleslogic_203',['JatekSzereplesLogic',['../class_game_info_1_1_logic_1_1_jatek_szereples_logic.html#acaef7ee715ba16e7e0b269a9463ff9fc',1,'GameInfo.Logic.JatekSzereplesLogic.JatekSzereplesLogic()'],['../class_game_info_1_1_logic_1_1_jatek_szereples_logic.html#a9f96ac272838cd3fc83ebfeac4889c78',1,'GameInfo.Logic.JatekSzereplesLogic.JatekSzereplesLogic(IJatekSzereplesekRepository jaszRepo)']]]
];
