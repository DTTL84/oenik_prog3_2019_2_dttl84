var searchData=
[
  ['data_159',['Data',['../namespace_game_info_1_1_data.html',1,'GameInfo']]],
  ['gameinfo_160',['GameInfo',['../namespace_game_info.html',1,'']]],
  ['interfaces_161',['Interfaces',['../namespace_game_info_1_1_repository_1_1_interfaces.html',1,'GameInfo::Repository']]],
  ['logic_162',['Logic',['../namespace_game_info_1_1_logic.html',1,'GameInfo']]],
  ['repository_163',['Repository',['../namespace_game_info_1_1_repository.html',1,'GameInfo']]],
  ['test_164',['Test',['../namespace_game_info_1_1_logic_1_1_test.html',1,'GameInfo::Logic']]]
];
