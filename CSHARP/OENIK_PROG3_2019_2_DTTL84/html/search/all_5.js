var searchData=
[
  ['icegrepository_48',['ICegRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_ceg_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['ijatekrepository_49',['IJatekRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_jatek_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['ijatekszereplesekrepository_50',['IJatekSzereplesekRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_jatek_szereplesek_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['ikarakterrepository_51',['IKarakterRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_karakter_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['ikiadasrepository_52',['IKiadasRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_kiadas_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['insertkarakter_53',['InsertKarakter',['../class_game_info_1_1_logic_1_1_karakter_logic.html#a4b4579efe7d6a67259dc68d92d5d3aac',1,'GameInfo::Logic::KarakterLogic']]],
  ['insertkiadas_54',['InsertKiadas',['../class_game_info_1_1_logic_1_1_kiadas_logic.html#a83a703eb2a0b2d1030c8440dfce8cf35',1,'GameInfo::Logic::KiadasLogic']]],
  ['insertnewceg_55',['InsertNewCeg',['../class_game_info_1_1_logic_1_1_ceg_logic.html#ae1dc9a0aedca80e188ba6ec95c8b7199',1,'GameInfo.Logic.CegLogic.InsertNewCeg()'],['../class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html#a78a2af9ce49e5848da4835ecbf170bfe',1,'GameInfo.Logic.Test.CegLogicTests.InsertNewCeg()']]],
  ['insertnewcegnull_56',['InsertNewCegNull',['../class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html#ab46e05f5b24e36bfa2e9f2f8c0f78035',1,'GameInfo::Logic::Test::CegLogicTests']]],
  ['insertnewjatek_57',['InsertNewJatek',['../class_game_info_1_1_logic_1_1_jatek_logic.html#a344f17cbfca7b410eaabbf688140113f',1,'GameInfo.Logic.JatekLogic.InsertNewJatek()'],['../class_game_info_1_1_logic_1_1_jatek_szereples_logic.html#a73d3d4b2849ada75d07fee222e8cd0ec',1,'GameInfo.Logic.JatekSzereplesLogic.InsertNewJatek()'],['../class_game_info_1_1_logic_1_1_test_1_1_jatek_logic_tests.html#a56c45dcb7cd6890f58a7595b3d8c031a',1,'GameInfo.Logic.Test.JatekLogicTests.InsertNewJatek()']]],
  ['irepository_58',['IRepository',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_3c_20cegek_20_3e_59',['IRepository&lt; Cegek &gt;',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_3c_20jatek_5fkiadasok_20_3e_60',['IRepository&lt; Jatek_Kiadasok &gt;',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_3c_20jatekok_20_3e_61',['IRepository&lt; Jatekok &gt;',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_3c_20jatekszereplesek_20_3e_62',['IRepository&lt; JatekSzereplesek &gt;',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]],
  ['irepository_3c_20karakterek_20_3e_63',['IRepository&lt; Karakterek &gt;',['../interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'GameInfo::Repository::Interfaces']]]
];
