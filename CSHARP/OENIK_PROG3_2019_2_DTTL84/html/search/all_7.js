var searchData=
[
  ['karakter_5fid_79',['Karakter_id',['../class_game_info_1_1_data_1_1_jatek_szereplesek.html#a4a9af3a50586aa278b49d1d6d3428cdc',1,'GameInfo.Data.JatekSzereplesek.Karakter_id()'],['../class_game_info_1_1_data_1_1_karakterek.html#a1b8697357938f63a49b931448e58972e',1,'GameInfo.Data.Karakterek.Karakter_id()']]],
  ['karakter_5fnev_80',['Karakter_nev',['../class_game_info_1_1_data_1_1_karakterek.html#ada9b4a28dc6004fa25c7ed04a30cba4b',1,'GameInfo::Data::Karakterek']]],
  ['karakterek_81',['Karakterek',['../class_game_info_1_1_data_1_1_karakterek.html',1,'GameInfo::Data']]],
  ['karaktereks_82',['Karaktereks',['../class_game_info_1_1_data_1_1_game_info_d_b_context.html#a4bbdec80ff8f7655b6c5cfe1914c7e0d',1,'GameInfo::Data::GameInfoDBContext']]],
  ['karakterhezcegek_83',['KarakterhezCegek',['../class_game_info_1_1_logic_1_1_ceg_logic.html#ad12895d837f43b3a1d050f452a6b01bb',1,'GameInfo::Logic::CegLogic']]],
  ['karakterlogic_84',['KarakterLogic',['../class_game_info_1_1_logic_1_1_karakter_logic.html',1,'GameInfo.Logic.KarakterLogic'],['../class_game_info_1_1_logic_1_1_karakter_logic.html#a363857450ba99bdadadd96ddbce815a9',1,'GameInfo.Logic.KarakterLogic.KarakterLogic()'],['../class_game_info_1_1_logic_1_1_karakter_logic.html#a500cda4709060fa49be09af27709f9ce',1,'GameInfo.Logic.KarakterLogic.KarakterLogic(IKarakterRepository karRepo)']]],
  ['karakterrepository_85',['KarakterRepository',['../class_game_info_1_1_repository_1_1_karakter_repository.html',1,'GameInfo.Repository.KarakterRepository'],['../class_game_info_1_1_repository_1_1_karakter_repository.html#a8d697a56bd7d9d18e5b740615392a054',1,'GameInfo.Repository.KarakterRepository.KarakterRepository()']]],
  ['keszito_5fid_86',['Keszito_id',['../class_game_info_1_1_data_1_1_jatekok.html#a629db71540f0961dc006618e7bdd80bf',1,'GameInfo::Data::Jatekok']]],
  ['kiadas_87',['Kiadas',['../class_game_info_1_1_data_1_1_jatek___kiadasok.html#ac0b42b31eee349ca483345b55204ea2e',1,'GameInfo::Data::Jatek_Kiadasok']]],
  ['kiadaslogic_88',['KiadasLogic',['../class_game_info_1_1_logic_1_1_kiadas_logic.html',1,'GameInfo.Logic.KiadasLogic'],['../class_game_info_1_1_logic_1_1_kiadas_logic.html#ab749a8678a780ae92cf965fc22db297f',1,'GameInfo.Logic.KiadasLogic.KiadasLogic()'],['../class_game_info_1_1_logic_1_1_kiadas_logic.html#aae8d142af3b3d91d647e37fa0fa07c09',1,'GameInfo.Logic.KiadasLogic.KiadasLogic(IKiadasRepository kiadasRepo)']]],
  ['kiadasrepository_89',['KiadasRepository',['../class_game_info_1_1_repository_1_1_kiadas_repository.html',1,'GameInfo.Repository.KiadasRepository'],['../class_game_info_1_1_repository_1_1_kiadas_repository.html#a6b0432f23548632918fca1a60c791280',1,'GameInfo.Repository.KiadasRepository.KiadasRepository()']]],
  ['kiado_5fid_90',['Kiado_id',['../class_game_info_1_1_data_1_1_jatekok.html#a8b68fda5fada539c40b8d45e87df5457',1,'GameInfo::Data::Jatekok']]]
];
