var hierarchy =
[
    [ "GameInfo.Data.Cegek", "class_game_info_1_1_data_1_1_cegek.html", null ],
    [ "GameInfo.Logic.CegLogic", "class_game_info_1_1_logic_1_1_ceg_logic.html", null ],
    [ "GameInfo.Logic.Test.CegLogicTests", "class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html", null ],
    [ "GameInfo.Crud", "class_game_info_1_1_crud.html", null ],
    [ "DbContext", null, [
      [ "GameInfo.Data.GameInfoDBContext", "class_game_info_1_1_data_1_1_game_info_d_b_context.html", null ]
    ] ],
    [ "GameInfo.Repository.Interfaces.IRepository< T >", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "GameInfo.Repository.Repository< T >", "class_game_info_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "GameInfo.Repository.Interfaces.IRepository< Cegek >", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "GameInfo.Repository.Interfaces.ICegRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_ceg_repository.html", [
        [ "GameInfo.Repository.CegRepository", "class_game_info_1_1_repository_1_1_ceg_repository.html", null ]
      ] ]
    ] ],
    [ "GameInfo.Repository.Interfaces.IRepository< Jatek_Kiadasok >", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "GameInfo.Repository.Interfaces.IKiadasRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_kiadas_repository.html", [
        [ "GameInfo.Repository.KiadasRepository", "class_game_info_1_1_repository_1_1_kiadas_repository.html", null ]
      ] ]
    ] ],
    [ "GameInfo.Repository.Interfaces.IRepository< Jatekok >", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "GameInfo.Repository.Interfaces.IJatekRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_jatek_repository.html", [
        [ "GameInfo.Repository.JatekRepository", "class_game_info_1_1_repository_1_1_jatek_repository.html", null ]
      ] ]
    ] ],
    [ "GameInfo.Repository.Interfaces.IRepository< JatekSzereplesek >", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "GameInfo.Repository.Interfaces.IJatekSzereplesekRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_jatek_szereplesek_repository.html", [
        [ "GameInfo.Repository.JatekSzereplesekRepository", "class_game_info_1_1_repository_1_1_jatek_szereplesek_repository.html", null ]
      ] ]
    ] ],
    [ "GameInfo.Repository.Interfaces.IRepository< Karakterek >", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "GameInfo.Repository.Interfaces.IKarakterRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_karakter_repository.html", [
        [ "GameInfo.Repository.KarakterRepository", "class_game_info_1_1_repository_1_1_karakter_repository.html", null ]
      ] ]
    ] ],
    [ "GameInfo.Data.Jatek_Kiadasok", "class_game_info_1_1_data_1_1_jatek___kiadasok.html", null ],
    [ "GameInfo.Logic.JatekLogic", "class_game_info_1_1_logic_1_1_jatek_logic.html", null ],
    [ "GameInfo.Logic.Test.JatekLogicTests", "class_game_info_1_1_logic_1_1_test_1_1_jatek_logic_tests.html", null ],
    [ "GameInfo.Data.Jatekok", "class_game_info_1_1_data_1_1_jatekok.html", null ],
    [ "GameInfo.Data.JatekSzereplesek", "class_game_info_1_1_data_1_1_jatek_szereplesek.html", null ],
    [ "GameInfo.Logic.JatekSzereplesLogic", "class_game_info_1_1_logic_1_1_jatek_szereples_logic.html", null ],
    [ "GameInfo.Data.Karakterek", "class_game_info_1_1_data_1_1_karakterek.html", null ],
    [ "GameInfo.Logic.KarakterLogic", "class_game_info_1_1_logic_1_1_karakter_logic.html", null ],
    [ "GameInfo.Logic.KiadasLogic", "class_game_info_1_1_logic_1_1_kiadas_logic.html", null ],
    [ "GameInfo.Menu", "class_game_info_1_1_menu.html", null ],
    [ "GameInfo.NonCrud", "class_game_info_1_1_non_crud.html", null ],
    [ "GameInfo.Repository.Repository< Cegek >", "class_game_info_1_1_repository_1_1_repository.html", [
      [ "GameInfo.Repository.CegRepository", "class_game_info_1_1_repository_1_1_ceg_repository.html", null ]
    ] ],
    [ "GameInfo.Repository.Repository< Jatek_Kiadasok >", "class_game_info_1_1_repository_1_1_repository.html", [
      [ "GameInfo.Repository.KiadasRepository", "class_game_info_1_1_repository_1_1_kiadas_repository.html", null ]
    ] ],
    [ "GameInfo.Repository.Repository< Jatekok >", "class_game_info_1_1_repository_1_1_repository.html", [
      [ "GameInfo.Repository.JatekRepository", "class_game_info_1_1_repository_1_1_jatek_repository.html", null ]
    ] ],
    [ "GameInfo.Repository.Repository< JatekSzereplesek >", "class_game_info_1_1_repository_1_1_repository.html", [
      [ "GameInfo.Repository.JatekSzereplesekRepository", "class_game_info_1_1_repository_1_1_jatek_szereplesek_repository.html", null ]
    ] ],
    [ "GameInfo.Repository.Repository< Karakterek >", "class_game_info_1_1_repository_1_1_repository.html", [
      [ "GameInfo.Repository.KarakterRepository", "class_game_info_1_1_repository_1_1_karakter_repository.html", null ]
    ] ]
];