var class_game_info_1_1_logic_1_1_karakter_logic =
[
    [ "KarakterLogic", "class_game_info_1_1_logic_1_1_karakter_logic.html#a363857450ba99bdadadd96ddbce815a9", null ],
    [ "KarakterLogic", "class_game_info_1_1_logic_1_1_karakter_logic.html#a500cda4709060fa49be09af27709f9ce", null ],
    [ "ChangePOP", "class_game_info_1_1_logic_1_1_karakter_logic.html#a56d64164d2d3a6b82fc462fa4070bedb", null ],
    [ "GetAllKarakter", "class_game_info_1_1_logic_1_1_karakter_logic.html#a6b978e6b8a5e9224eb17b4ffda996af5", null ],
    [ "GetNevekSorozatbol", "class_game_info_1_1_logic_1_1_karakter_logic.html#a1e4770f8b53d5e945ed5b558094a1d44", null ],
    [ "GetOneKarakter", "class_game_info_1_1_logic_1_1_karakter_logic.html#a89c1320bbca7bd122be4f13067c18fc4", null ],
    [ "InsertKarakter", "class_game_info_1_1_logic_1_1_karakter_logic.html#a4b4579efe7d6a67259dc68d92d5d3aac", null ],
    [ "RemoveKarakter", "class_game_info_1_1_logic_1_1_karakter_logic.html#ad4261fd2668627af70b966362035eb5f", null ]
];