var class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests =
[
    [ "DeleteCeg", "class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html#ab14197f6e849d30cddd57e1ce2361529", null ],
    [ "GetAll", "class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html#aa04b1fe3ed9622902f596e8b65b7b610", null ],
    [ "GetOneCeg", "class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html#a7968ddf1877f6146b682afb5fea97adf", null ],
    [ "InsertNewCeg", "class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html#a78a2af9ce49e5848da4835ecbf170bfe", null ],
    [ "InsertNewCegNull", "class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html#ab46e05f5b24e36bfa2e9f2f8c0f78035", null ],
    [ "NonCrud", "class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html#aaa120e1f00bdef7b068995927d4fb862", null ],
    [ "SetUpRepository", "class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html#aadccf1bc4386715393142f3f423fa2fe", null ],
    [ "UpdateCeg", "class_game_info_1_1_logic_1_1_test_1_1_ceg_logic_tests.html#aeb9cdd07f54c2cfb37fc9b216a579ef3", null ]
];