var class_game_info_1_1_logic_1_1_jatek_logic =
[
    [ "JatekLogic", "class_game_info_1_1_logic_1_1_jatek_logic.html#ab36cb8012824b9a70834fd8c0b72b727", null ],
    [ "JatekLogic", "class_game_info_1_1_logic_1_1_jatek_logic.html#a566fb33cf8e8aa95c26a89d96b0a3bdc", null ],
    [ "ChangeNev", "class_game_info_1_1_logic_1_1_jatek_logic.html#a9d4e0d6a75b35a75b52b9ed1e5b98561", null ],
    [ "GetAdottCegAltalKeszitettJatekok", "class_game_info_1_1_logic_1_1_jatek_logic.html#a06b16fe72e5daedf9d58323a493d9309", null ],
    [ "GetAllGames", "class_game_info_1_1_logic_1_1_jatek_logic.html#a74c36001af8207326c6c24e437205894", null ],
    [ "GetOneGame", "class_game_info_1_1_logic_1_1_jatek_logic.html#a018c8b095ccde8a749c93d07939ee26a", null ],
    [ "InsertNewJatek", "class_game_info_1_1_logic_1_1_jatek_logic.html#a344f17cbfca7b410eaabbf688140113f", null ],
    [ "JatekEltavolitasa", "class_game_info_1_1_logic_1_1_jatek_logic.html#a0ee679326c11af28d9fbc41d9409afd9", null ]
];