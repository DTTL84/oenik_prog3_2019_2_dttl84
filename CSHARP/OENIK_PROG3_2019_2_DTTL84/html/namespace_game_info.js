var namespace_game_info =
[
    [ "Data", "namespace_game_info_1_1_data.html", "namespace_game_info_1_1_data" ],
    [ "Logic", "namespace_game_info_1_1_logic.html", "namespace_game_info_1_1_logic" ],
    [ "Repository", "namespace_game_info_1_1_repository.html", "namespace_game_info_1_1_repository" ],
    [ "Crud", "class_game_info_1_1_crud.html", "class_game_info_1_1_crud" ],
    [ "Menu", "class_game_info_1_1_menu.html", null ],
    [ "NonCrud", "class_game_info_1_1_non_crud.html", "class_game_info_1_1_non_crud" ]
];