var namespace_game_info_1_1_repository =
[
    [ "Interfaces", "namespace_game_info_1_1_repository_1_1_interfaces.html", "namespace_game_info_1_1_repository_1_1_interfaces" ],
    [ "CegRepository", "class_game_info_1_1_repository_1_1_ceg_repository.html", "class_game_info_1_1_repository_1_1_ceg_repository" ],
    [ "JatekRepository", "class_game_info_1_1_repository_1_1_jatek_repository.html", "class_game_info_1_1_repository_1_1_jatek_repository" ],
    [ "JatekSzereplesekRepository", "class_game_info_1_1_repository_1_1_jatek_szereplesek_repository.html", "class_game_info_1_1_repository_1_1_jatek_szereplesek_repository" ],
    [ "KarakterRepository", "class_game_info_1_1_repository_1_1_karakter_repository.html", "class_game_info_1_1_repository_1_1_karakter_repository" ],
    [ "KiadasRepository", "class_game_info_1_1_repository_1_1_kiadas_repository.html", "class_game_info_1_1_repository_1_1_kiadas_repository" ],
    [ "Repository", "class_game_info_1_1_repository_1_1_repository.html", "class_game_info_1_1_repository_1_1_repository" ]
];