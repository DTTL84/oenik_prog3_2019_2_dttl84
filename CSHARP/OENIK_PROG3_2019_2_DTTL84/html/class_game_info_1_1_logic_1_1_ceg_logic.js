var class_game_info_1_1_logic_1_1_ceg_logic =
[
    [ "CegLogic", "class_game_info_1_1_logic_1_1_ceg_logic.html#aefb7484bc97213414f6d87c4daa07875", null ],
    [ "CegLogic", "class_game_info_1_1_logic_1_1_ceg_logic.html#a87d0249005e3b3007efbb72474e46ed4", null ],
    [ "ChangeCegFonok", "class_game_info_1_1_logic_1_1_ceg_logic.html#a6d5eeecc6deff85fc257632d4081cba3", null ],
    [ "GetAllCegek", "class_game_info_1_1_logic_1_1_ceg_logic.html#abeb3992f133bcca3ceeb1a28e09e9f5e", null ],
    [ "GetOneCeg", "class_game_info_1_1_logic_1_1_ceg_logic.html#a9c6a1c309f8e2f64d1c67af88f642e7d", null ],
    [ "InsertNewCeg", "class_game_info_1_1_logic_1_1_ceg_logic.html#ae1dc9a0aedca80e188ba6ec95c8b7199", null ],
    [ "KarakterhezCegek", "class_game_info_1_1_logic_1_1_ceg_logic.html#ad12895d837f43b3a1d050f452a6b01bb", null ],
    [ "RemoveCeg", "class_game_info_1_1_logic_1_1_ceg_logic.html#a8cc4d0f08453c0ce2fa1159d4b5192a1", null ]
];