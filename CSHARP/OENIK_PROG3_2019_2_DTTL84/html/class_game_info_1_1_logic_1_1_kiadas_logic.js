var class_game_info_1_1_logic_1_1_kiadas_logic =
[
    [ "KiadasLogic", "class_game_info_1_1_logic_1_1_kiadas_logic.html#ab749a8678a780ae92cf965fc22db297f", null ],
    [ "KiadasLogic", "class_game_info_1_1_logic_1_1_kiadas_logic.html#aae8d142af3b3d91d647e37fa0fa07c09", null ],
    [ "ChangeKiadas", "class_game_info_1_1_logic_1_1_kiadas_logic.html#a29e06dc13e9b2aa68b82513eb60fca22", null ],
    [ "GetAllKiadas", "class_game_info_1_1_logic_1_1_kiadas_logic.html#afcb4eed5f36643c00b44088bcb34740f", null ],
    [ "GetOneKiadas", "class_game_info_1_1_logic_1_1_kiadas_logic.html#a5aa43d060c464523036d828a571ee5d6", null ],
    [ "InsertKiadas", "class_game_info_1_1_logic_1_1_kiadas_logic.html#a83a703eb2a0b2d1030c8440dfce8cf35", null ],
    [ "RemoveKiadas", "class_game_info_1_1_logic_1_1_kiadas_logic.html#aae139f7c17d36d7bfa5d9705aeda05e7", null ]
];