var class_game_info_1_1_logic_1_1_test_1_1_jatek_logic_tests =
[
    [ "DeleteJatek", "class_game_info_1_1_logic_1_1_test_1_1_jatek_logic_tests.html#aa2783a9c3482b8fefd9d36bbe855655a", null ],
    [ "GetAll", "class_game_info_1_1_logic_1_1_test_1_1_jatek_logic_tests.html#ac02dd93731d01ad071ce57df1847d688", null ],
    [ "GetOnJatek", "class_game_info_1_1_logic_1_1_test_1_1_jatek_logic_tests.html#a29dcd3bac620fed630ab03bd4a317622", null ],
    [ "InsertNewJatek", "class_game_info_1_1_logic_1_1_test_1_1_jatek_logic_tests.html#a56c45dcb7cd6890f58a7595b3d8c031a", null ],
    [ "NonCrudTest", "class_game_info_1_1_logic_1_1_test_1_1_jatek_logic_tests.html#aaad4b2c45ac4bcf3b26276e52ccdd891", null ],
    [ "SetUpRepository", "class_game_info_1_1_logic_1_1_test_1_1_jatek_logic_tests.html#a3bf7603bfef01309a2a9b8d49baa327b", null ],
    [ "UpdateJatek", "class_game_info_1_1_logic_1_1_test_1_1_jatek_logic_tests.html#aec4ede7edf766af47c0a82d3d92d1723", null ]
];