var namespace_game_info_1_1_repository_1_1_interfaces =
[
    [ "ICegRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_ceg_repository.html", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_ceg_repository" ],
    [ "IJatekRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_jatek_repository.html", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_jatek_repository" ],
    [ "IJatekSzereplesekRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_jatek_szereplesek_repository.html", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_jatek_szereplesek_repository" ],
    [ "IKarakterRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_karakter_repository.html", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_karakter_repository" ],
    [ "IKiadasRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_kiadas_repository.html", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_kiadas_repository" ],
    [ "IRepository", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository.html", "interface_game_info_1_1_repository_1_1_interfaces_1_1_i_repository" ]
];