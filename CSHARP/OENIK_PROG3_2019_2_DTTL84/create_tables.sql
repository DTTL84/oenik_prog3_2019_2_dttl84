﻿Create TABLE Karakterek(
  Karakter_id INT PRIMARY KEY identity(1,1),
  Karakter_nev VARCHAR(50),
  Faj VARCHAR(20) NOT NULL,
  Nem VARCHAR(7) NOT NULL,
  szulohely VARCHAR(20),
  POP VARCHAR(3) NOT NULL,
  CONSTRAINT nem_ck CHECK(Nem IN( 'male', 'female')),
  CONSTRAINT pop_ck CHECK(POP IN( 'yes', 'no')),
);

CREATE TABLE Cegek(
  Ceg_id INT PRIMARY KEY identity(1,1),
  Ceg_nev VARCHAR(80) UNIQUE,
  Szekhely VARCHAR(30),
  Alapitas DATETIME,
  Fonok VARCHAR(60),
  Anyaceg INT,
  Dolgozok_szama INT,
  CONSTRAINT FK_Anyaceg FOREIGN KEY (Anyaceg) REFERENCES Cegek (Ceg_id)
);

Create TABLE Jatekok(
  Jatek_id INT PRIMARY KEY identity(1,1),
  Jatek_nev VARCHAR(50),
  Pegi SMALLINT,
  Keszito_id INT,
  Kiado_id INT,
  Leiras VARCHAR(200) UNIQUE,
  Max_players INT,
  Game_engine VARCHAR(20),
  Sorozat VARCHAR(50),
  CONSTRAINT FK_Keszito FOREIGN KEY (Keszito_id) REFERENCES Cegek (Ceg_id),
  CONSTRAINT FK_Kiado FOREIGN KEY (Kiado_id) REFERENCES Cegek (Ceg_id)
);

CREATE TABLE Tipusok(
  Tipus_id INT PRIMARY KEY identity(1,1),
  Tipus_nev VARCHAR(20) UNIQUE
);

CREATE TABLE JatekTipusai(
  Jatek_id INT,
  Tipus_id INT NOT NULL,
  CONSTRAINT FK_Jatek_id FOREIGN KEY (Jatek_id) REFERENCES Jatekok (Jatek_id),
  CONSTRAINT FK_Tipus_id FOREIGN KEY (Tipus_id) REFERENCES Tipusok (Tipus_id)
);

CREATE TABLE Jatek_Kiadasok(
  Jatek_id INT,
  Platform VARCHAR(20),
  Kiadas DATE,
  Tarhely INT,
  CONSTRAINT jatek_kiadas_pk PRIMARY KEY (Jatek_id, Platform)
);

CREATE TABLE JatekSzereplesek(
  Jatek_id INT,
  Karakter_id INT NOT NULL,
  CONSTRAINT FK_Jatek_id_jatekSzereples FOREIGN KEY (Jatek_id) REFERENCES Jatekok(Jatek_id),
  CONSTRAINT FK_Karakter_id_jatekSzereples FOREIGN KEY (Karakter_id) REFERENCES Karakterek(Karakter_id)
);
