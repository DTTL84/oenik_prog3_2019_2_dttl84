﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameInfo.Web.Models
{
    public class CompanyViewModel
    {
        public Company EditedCompany { get; set; }
        public List<Company> ListOfCompanies { get; set; }
    }
}