﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameInfo.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GameInfo.Data.Cegek, GameInfo.Web.Models.Company>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.Ceg_id)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Ceg_nev)).
                    ForMember(dest => dest.Szekhely, map => map.MapFrom(src => src.Szekhely)).
                    ForMember(dest => dest.MotherCompany, map => map.MapFrom(src => 
                        src.Anyaceg == null ? "" : "asd")).
                    ForMember(dest => dest.Boss, map => map.MapFrom(src => src.Fonok)).
                    ForMember(dest => dest.Alapitas, map => map.MapFrom(src => src.Alapitas)).
                    ForMember(dest => dest.Employees, map => map.MapFrom(src => src.Dolgozok_szama));
            });

            return config.CreateMapper();
        }
    }
}