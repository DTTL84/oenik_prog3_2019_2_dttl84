﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameInfo.Web.Models
{
    public class Company
    {
        [Display(Name = "Company Id")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Company Name")]
        [Required]
        [StringLength(100, MinimumLength = 5)]
        public string Name { get; set; }

        [Display(Name = "Company Szekhely")]
        [Required]
        public string Szekhely { get; set; }

        [Display(Name = "Company Alapitas")]
        [Required]
        public DateTime Alapitas { get; set; }

        [Display(Name = "Company's Boss")]
        [Required]
        public string Boss { get; set; }

        [Display(Name = "Company's MotherCompany")]
        public string MotherCompany { get; set; }

        [Display(Name = "Number of employees")]
        [Required]
        public int Employees { get; set; }
    }
}