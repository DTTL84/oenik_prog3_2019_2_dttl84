﻿using AutoMapper;
using GameInfo.Data;
using GameInfo.Logic;
using GameInfo.Repository;
using GameInfo.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static GameInfo.Logic.CegLogic;

namespace GameInfo.Web.Controllers
{
    public class CompaniesController : Controller
    {
        ICegLogic logic;
        IMapper mapper;
        CompanyViewModel vm;

        public CompaniesController()
        {
            GameInfoDBContext ctx = new GameInfoDBContext();
            CegRepository cegRepo = new CegRepository(ctx);
            logic = new CegLogic(cegRepo);
            mapper = MapperFactory.CreateMapper();

            vm = new CompanyViewModel();
            vm.EditedCompany = new Company();
            var companies = logic.GetAllCompanies();
            vm.ListOfCompanies = mapper.Map<IList<Data.Cegek>, List<Models.Company>>(companies);
        }

        private Company GetCompanyModel(int id)
        {
            Cegek oneC = logic.GetOneCeg(id);
            return mapper.Map<Data.Cegek, Models.Company>(oneC);
        }

        // GET: Companies
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("CompanyIndex", vm);
        }

        // GET: Companies/Details/5
        public ActionResult Details(int id)
        {
            return View("CompanyDetails", GetCompanyModel(id));
        }

        // GET: Companies/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedCompany = GetCompanyModel(id);
            return View("CompanyIndex", vm);
        }

        // POST: Companies/Edit/5
        [HttpPost]
        public ActionResult Edit(Company comp, string editAction)
        {
            if (ModelState.IsValid && comp != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    /*
                    Cegek newCeg = new Cegek()
                    {
                        Ceg_nev = nev,
                        Szekhely = hely,
                        Fonok = fonok,
                        Anyaceg = anya,
                        Dolgozok_szama = szam,
                        Alapitas = new DateTime(ev, honap, nap),
                    };
                    logic.InsertNewCeg(newCeg);*/
                    //logic.ChangeCompany();

                    
                    logic.InsertNewCeg(new Cegek()
                    {
                        Alapitas = new DateTime(comp.Alapitas.Year, comp.Alapitas.Month, comp.Alapitas.Day),
                        Anyaceg = 2,
                        Ceg_nev = comp.Name,
                        Fonok = comp.Boss,
                        Szekhely = comp.Szekhely,
                        Dolgozok_szama = comp.Employees
                    });
                }
                else
                {
                    bool success = logic.ChangeCompany( 
                        comp.Id,
                        new DateTime(comp.Alapitas.Year, comp.Alapitas.Month, comp.Alapitas.Day),
                        comp.Name,
                        comp.Employees,
                        comp.Boss,
                        comp.Szekhely
                    );
                    if (!success)
                    {
                        TempData["editResult"] = "Edit Fail";
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedCompany = comp;
                return View("CompanyIndex", vm);
            }
        }

        // GET: Companies/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }
        
        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete Fail";
            if (logic.RemoveCompany(id))
            {
                TempData["editResult"] = "Delete OK";
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
