﻿// <copyright file="JatekSzereplesekRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Characters in games Repo.
    /// </summary>
    public class JatekSzereplesekRepository : Repository<JatekSzereplesek>, IJatekSzereplesekRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JatekSzereplesekRepository"/> class.
        /// </summary>
        /// <param name="ctx">Database context.</param>
        public JatekSzereplesekRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Remove Characters in games.
        /// </summary>
        /// <param name="jid">Game id.</param>
        /// <param name="kid">Character id.</param>
        public void RemoveSzereples(int jid, int kid)
        {
            var szerpeles = this.GetOne(jid, kid);
            this.ctx.Set<JatekSzereplesek>().Attach(szerpeles);
            this.ctx.Set<JatekSzereplesek>().Remove(szerpeles);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Get one Characters in games.
        /// </summary>
        /// <param name="jid">Game id.</param>
        /// <param name="kid">Character id.</param>
        /// <returns>1 row Characters in games.</returns>
        public JatekSzereplesek GetOne(int jid, int kid)
        {
            return this.GetAll().FirstOrDefault(x => x.Jatek_id.Equals(jid) && x.Karakter_id.Equals(kid));
        }
    }
}
