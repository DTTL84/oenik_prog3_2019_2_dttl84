﻿// <copyright file="IJatekSzereplesekRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;

    /// <summary>
    /// Characters in games interface.
    /// </summary>
    public interface IJatekSzereplesekRepository : IRepository<JatekSzereplesek>
    {
        /// <summary>
        /// Remove one Characters in games.
        /// </summary>
        /// <param name="jid">Game id.</param>
        /// <param name="kid">Character id.</param>
        void RemoveSzereples(int jid, int kid);
    }
}
