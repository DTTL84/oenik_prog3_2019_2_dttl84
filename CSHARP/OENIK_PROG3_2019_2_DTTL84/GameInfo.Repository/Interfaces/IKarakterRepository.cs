﻿// <copyright file="IKarakterRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;

    /// <summary>
    /// Character Repository interface.
    /// </summary>
    public interface IKarakterRepository : IRepository<Karakterek>
    {
        /// <summary>
        /// Remove Character.
        /// </summary>
        /// <param name="id">Character id.</param>
        void RemoveKarakter(int id);

        /// <summary>
        /// Change POP.
        /// </summary>
        /// <param name="id">Character id.</param>
        /// <param name="newPOP">new value.</param>
        void ChangePOP(int id, string newPOP);

        /// <summary>
        /// Get one Character.
        /// </summary>
        /// <param name="id">Character id.</param>
        /// <returns>1 row Character.</returns>
        Karakterek GetOne(int id);
    }
}
