﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Repository interface.
    /// </summary>
    /// <typeparam name="T">T osztály.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Return all T type.
        /// </summary>
        /// <returns>All T.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Add T entity.
        /// </summary>
        /// <param name="entity">T entity.</param>
        void Add(T entity);
    }
}
