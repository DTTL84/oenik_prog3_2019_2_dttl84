﻿// <copyright file="IKiadasRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;

    /// <summary>
    /// ComeOut Repository interface.
    /// </summary>
    public interface IKiadasRepository : IRepository<Jatek_Kiadasok>
    {
        /// <summary>
        /// Delete ComeOut.
        /// </summary>
        /// <param name="jid">Game id.</param>
        /// <param name="platform">Platfoprm.</param>
        void RemoveKiadas(int jid, string platform);

        /// <summary>
        /// Change ComeOut date.
        /// </summary>
        /// <param name="jid">Game id.</param>
        /// <param name="platform">Platform.</param>
        /// <param name="newDate">New Date.</param>
        void ChangeKiadas(int jid, string platform, DateTime newDate);

        /// <summary>
        /// Return one ComeOut.
        /// </summary>
        /// <param name="id">Game id.</param>
        /// <param name="platform">Platform.</param>
        /// <returns>New ComeOut.</returns>
        Jatek_Kiadasok GetOne(int id, string platform);
    }
}
