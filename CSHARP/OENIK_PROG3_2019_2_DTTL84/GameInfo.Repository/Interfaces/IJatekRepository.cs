﻿// <copyright file="IJatekRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;

    public interface IJatekRepository : IRepository<Jatekok>
    {
        /// <summary>
        /// Remove a Game.
        /// </summary>
        /// <param name="id">Játék id-ja.</param>
        void RemoveJatek(int id);

        /// <summary>
        /// Change name.
        /// </summary>
        /// <param name="id">Game id.</param>
        /// <param name="newNev">New name.</param>
        void ChangeNev(int id, string newNev);

        /// <summary>
        /// Get one Game.
        /// </summary>
        /// <param name="id"> Game id.</param>
        /// <returns> 1 row Game.</returns>
        Jatekok GetOne(int id);
    }
}
