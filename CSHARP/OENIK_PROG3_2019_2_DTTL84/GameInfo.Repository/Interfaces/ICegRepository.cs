﻿// <copyright file="ICegRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;

    /// <summary>
    /// Company Repository interface.
    /// </summary>
    public interface ICegRepository : IRepository<Cegek>
    {
        /// <summary>
        /// Delete Company.
        /// </summary>
        /// <param name="id">Company id.</param>
        bool RemoveCompany(int id);

        /// <summary>
        /// Change boss name.
        /// </summary>
        /// <param name="id">Company id.</param>
        /// <param name="newFonok">The new boss name.</param>
        void ChangeFonok(int id, string newFonok);

        bool ChangeCompany(int id, DateTime alapitas, string newName, int newEmployees, string newBoss, string newSzekhely);

        /// <summary>
        /// Company.
        /// </summary>
        /// <param name="id">Company id.</param>
        /// <returns>1 row Company.</returns>
        Cegek GetOne(int id);
    }
}