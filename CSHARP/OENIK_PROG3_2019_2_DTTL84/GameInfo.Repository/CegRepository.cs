﻿// <copyright file="CegRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Company Repository.
    /// </summary>
    public class CegRepository : Repository<Cegek>, ICegRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CegRepository"/> class.
        /// </summary>
        /// <param name="ctx"> Database context.</param>
        public CegRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Change boss.
        /// </summary>
        /// <param name="id">Company id.</param>
        /// <param name="newFonok">New boss's name.</param>
        public void ChangeFonok(int id, string newFonok)
        {
            var ceg = this.GetOne(id);
            ceg.Fonok = newFonok;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Remove Company.
        /// </summary>
        /// <param name="id">Company id.</param>
        public void RemoveCeg(int id)
        {
            var ceg = this.GetOne(id);
            this.ctx.Set<Cegek>().Attach(ceg);
            this.ctx.Set<Cegek>().Remove(ceg);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Get one row.
        /// </summary>
        /// <param name="id">Company id.</param>
        /// <returns>1 row Company.</returns>
        public Cegek GetOne(int id)
        {
            return this.GetAll().FirstOrDefault(x => x.Ceg_id == id);
        }

        public bool RemoveCompany(int id)
        {
            if (id == 0 || id == null)
            {
                return false;
            }
            var ceg = this.GetOne(id);
            this.ctx.Set<Cegek>().Attach(ceg);
            this.ctx.Set<Cegek>().Remove(ceg);
            this.ctx.SaveChanges();
            return true;
        }

        public bool ChangeCompany(int id, DateTime alapitas, string newName, int newEmployees, string newBoss, string newSzekhely)
        {
            Cegek entity = this.GetOne(id);
            if (entity == null)
            {
                return false;
            }

            entity.Alapitas = alapitas;
            entity.Ceg_nev = newName;
            entity.Dolgozok_szama = newEmployees;
            entity.Fonok = newBoss;
            entity.Szekhely = newSzekhely;
            ctx.SaveChanges();
            return true;
        }
    }
}
