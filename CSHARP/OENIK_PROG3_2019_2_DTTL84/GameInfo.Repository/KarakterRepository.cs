﻿// <copyright file="KarakterRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Character Repository.
    /// </summary>
    public class KarakterRepository : Repository<Karakterek>, IKarakterRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KarakterRepository"/> class.
        /// Character Repo.
        /// </summary>
        /// <param name="ctx">Database context.</param>
        public KarakterRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Change POP.
        /// </summary>
        /// <param name="id">Character id-ja.</param>
        /// <param name="newPOP">New value.</param>
        public void ChangePOP(int id, string newPOP)
        {
            var pop = this.GetOne(id);
            pop.POP = newPOP;
            this.ctx.SaveChanges();
        }

        /*
        /// <summary>
        /// Karakter beillesztése.
        /// </summary>
        /// <param name="newKarakter">Új karakter.</param>
        public void InsertKarakter(Karakterek newKarakter)
        {
            this.ctx.Set<Karakterek>().Attach(newKarakter);
            this.ctx.Set<Karakterek>().Add(newKarakter);
            this.ctx.SaveChanges();
        }*/

        /// <summary>
        /// Delete Character.
        /// </summary>
        /// <param name="id">Character id.</param>
        public void RemoveKarakter(int id)
        {
            var karakter = this.GetOne(id);
            this.ctx.Set<Karakterek>().Attach(karakter);
            this.ctx.Set<Karakterek>().Remove(karakter);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Get one Character.
        /// </summary>
        /// <param name="id">Character id.</param>
        /// <returns>1 row Character.</returns>
        public Karakterek GetOne(int id)
        {
            return this.GetAll().FirstOrDefault(x => x.Karakter_id.Equals(id));
        }
    }
}
