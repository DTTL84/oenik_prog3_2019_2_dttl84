﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Repository.
    /// </summary>
    /// <typeparam name="T">T IRepository.</typeparam>
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Database context.
        /// </summary>
        protected readonly DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="ctx">Database Context.</param>
        public Repository(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Add one T entity.
        /// </summary>
        /// <param name="entity">T type class.</param>
        public void Add(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("This is null");
            }

            this.ctx.Set<T>().Add(entity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Get all T entity.
        /// </summary>
        /// <returns>All T.</returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }
    }
}
