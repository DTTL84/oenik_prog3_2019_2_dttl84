﻿// <copyright file="JatekRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Game Repo.
    /// </summary>
    public class JatekRepository : Repository<Jatekok>, IJatekRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JatekRepository"/> class.
        /// </summary>
        /// <param name="ctx">GameInfoDatabase Context.</param>
        public JatekRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Remove Game.
        /// </summary>
        /// <param name="id">Game id.</param>
        public void RemoveJatek(int id)
        {
            var jatek = this.GetOne(id);
            this.ctx.Set<Jatekok>().Attach(jatek);
            this.ctx.Set<Jatekok>().Remove(jatek);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Change name.
        /// </summary>
        /// <param name="id">Game id.</param>
        /// <param name="newNev">New name.</param>
        public void ChangeNev(int id, string newNev)
        {
            var nev = this.GetOne(id);
            nev.Jatek_nev = newNev;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Get one row.
        /// </summary>
        /// <param name="id">Company's id.</param>
        /// <returns>1 Company.</returns>
        public Jatekok GetOne(int id)
        {
            return this.GetAll().FirstOrDefault(x => x.Jatek_id.Equals(id));
        }
    }
}
