﻿// <copyright file="KiadasRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameInfo.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameInfo.Data;
    using GameInfo.Repository.Interfaces;

    /// <summary>
    /// Come out Repository.
    /// </summary>
    public class KiadasRepository : Repository<Jatek_Kiadasok>, IKiadasRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KiadasRepository"/> class.
        /// </summary>
        /// <param name="ctx"> Database Context.</param>
        public KiadasRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Change Come out date.
        /// </summary>
        /// <param name="jid">Game id.</param>
        /// <param name="platform">Platform.</param>
        /// <param name="newDate">New date.</param>
        public void ChangeKiadas(int jid, string platform, DateTime newDate)
        {
            var kiadas = this.GetOne(jid, platform);
            kiadas.Kiadas = newDate;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Delete Come out.
        /// </summary>
        /// <param name="jid">Game id.</param>
        /// <param name="platform">Platform.</param>
        public void RemoveKiadas(int jid, string platform)
        {
            var kiadas = this.GetOne(jid, platform);
            this.ctx.Set<Jatek_Kiadasok>().Attach(kiadas);
            this.ctx.Set<Jatek_Kiadasok>().Remove(kiadas);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Get one Come out.
        /// </summary>
        /// <param name="jid">Game id.</param>
        /// <param name="platform">Platform.</param>
        /// <returns>1 row Come out.</returns>
        public Jatek_Kiadasok GetOne(int jid, string platform)
        {
            return this.GetAll().FirstOrDefault(x => x.Jatek_id.Equals(jid) && x.Platform.Equals(platform));
        }
    }
}
